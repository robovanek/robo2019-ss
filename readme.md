# DuctTape @ Robosoutěž 2019

This is the program used by the team DuctTape on the [Robosoutěž 2019][rs1] robot competition.
The rules and the task can be found [here][rs2] (in Czech).

[rs1]: https://robosoutez.fel.cvut.cz/robosoutez-2019-pro-stredoskolske-tymy
[rs2]: https://robosoutez.fel.cvut.cz/zadani-soutezni-ulohy-mountain-climber-0

## Checking it out

The program is built on the [ev3dev][ev3dev-home] operating system (stretch version).
Installation guide can be found [here][ev3dev-install].

[ev3dev-home]: https://www.ev3dev.org/
[ev3dev-install]: https://www.ev3dev.org/docs/getting-started/


Prerequisites for development:

0. Use Ubuntu.

### How to compile

Compilation is tested only on the PC/x86 platform, however it should
work everywhere where the necessary packages are available.

#### For PC/x86

1. Install the necessary packages (example provided for Ubuntu):
  ```sh
  sudo apt install cmake            \
                   build-essential  \
                   pkg-config       \
                   libncurses5-dev  \
                   libudev-dev      \
                   qtbase5-dev      \
                   openssh-client
  ```

2. Create a build directory
  ```sh
  cd <repo>/prg
  mkdir build
  cd build
  ```

3. Prepare build with CMake
  ```sh
  cmake .. -DCMAKE_BUILD_TYPE=Debug
  ```

4. Run build
  ```sh
  # for everything
  make all

  # for robot program only
  make ducttape

  # for gui only
  make dtgui
  ```

#### For Brick/ARM

1. Install Docker CE: [guide here](https://docs.docker.com/install/linux/docker-ce/ubuntu/#install-docker-engine---community)

2. Build the container image used for compiling the program:
  ```sh
  sudo docker build -t linuxtardis:robo2019 <repo>/docker
  ```

3. Use the provided build scripts:
  ```sh
  <repo>/script/rcmake.sh debug
  <repo>/script/rbuild.sh
  ```

### How to prepare the brick & upload

1. Disable predictable interface naming: [guide here](https://serverfault.com/a/741212)

2. Connect the brick to the computer via a USB cable.

3. On the brick, go to the network connections and try to connect to the Wired connection.
   It will eventually fail - don't worry, this will be resolved shortly.

4. In NetworkManager, edit the new connection.
    * Optionally rename the connection using the top textbox.
    * Go to the IPv4 tab and select Shared to other computers.
    * Go to the IPv6 tab and select Ignored.
    * Save the connection.

4. Disconnect the brick from the computer and then connect it back.

5. On the brick, try to connect again to the Wired connection. It should succeed this time.

6. On the PC, add an alias to `.ssh/config` named `ducttape` pointing to the IP address of
   the brick (shown on the brick in the upper left corner):
  ```
  Host ducttape
    HostName 10.42.0.2
    User robot
  ```

7. Generate a new SSH key IF you don't already have one. Guide can be found [here][gitssh].

8. Copy the public part of the SSH key to the brick:
  ```sh
  ssh-copy-id ducttape
  # you will be asked for password, the default is maker
  ```

9. Connect to the brick:
  ```sh
  ssh ducttape
  ```

10. Change the default password by running this in the SSH session:
  ```sh
  passwd
  ```

11. Allow passwordless sudo (necessary for uploading) by running this in the SSH session:
  ```sh
  sudo visudo
  ```
  In the editor, it is necessary to change the line starting with `%sudo` to look like this:
  ```
  %sudo   ALL=(ALL:ALL) NOPASSWD: ALL
  ```

12. Install the necessary packages on the brick by running this in the SSH session:
  ```sh
  sudo apt-get update
  sudo apt-get install libcap2-bin gdbserver libncurses5-dev
  ```

13. Create the program configuration directory by runing this in the SSH session:
  ```sh
  mkdir ~/etc
  ```

14. Now you can close the SSH session:
  ```sh
  exit
  ```

15. Let's upload the program. Just run the upload script (it is necessary to have the program built using the rbuild.sh script):
  ```sh
  <repo>/script/rupload.sh
  ```

16. Let's also upload the configuration:
  ```sh
  scp <repo>/prg/ducttape.properties ducttape:etc
  ```

The program should now be ready.

[gitssh]: https://help.github.com/en/github/authenticating-to-github/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent

### How to debug

Follow the guide in `<repo>/prg/debugging.md`

If you are using CLion, you can additionally set up in-IDE debugging.

* Add a new remote gdb debug configuration.
* Set `10.42.0.2:6666` as the target.
* Set `<repo>/prg/build/ducttape/ducttape.dbg` as the symbol file.
* Set sysroot to the path where you have extracted the sysroot.
* Add a path mapping from `/home/compiler/program` to `<repo>/prg`

