#ifndef DUCTTAPE_HILL_MAP_HPP
#define DUCTTAPE_HILL_MAP_HPP

#include <robofat2/maps/tile_map.hpp>

namespace ducttape {
    namespace maps {
        using namespace robofat2::maps;

        struct tile {
        public:
            static constexpr int height_min = 0;
            static constexpr int height_max = 6;
            static constexpr int height_floor = height_min;

            explicit tile();
            explicit tile(bool real);
            explicit tile(bool real, int height);
            explicit tile(bool real, int lowerBound, int upperBound);

            int get_lower_bound() const;
            int get_upper_bound() const;
            bool is_real() const;

            tile with_lower_bound(int value);
            tile with_upper_bound(int value);
            tile with_height(int value);

            std::string as_string() const;

        private:
            int m_heightMin;
            int m_heightMax;
            bool m_real;
        };

        class hill_map : public tile_map<tile> {
        public:
            hill_map();

        private:
            void drawInitial();
        };

        bool is_reachable(tile start, tile end);
    }
}

#endif //DUCTTAPE_HILL_MAP_HPP
