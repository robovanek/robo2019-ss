#ifndef DUCTTAPE_HILL_DIJKSTRA_HPP
#define DUCTTAPE_HILL_DIJKSTRA_HPP


#include <robofat2/maps/dijkstra.hpp>
#include "hill_map.hpp"

namespace ducttape {
    namespace maps {
        using robofat2::maps::dijkstra_meta;
        using robofat2::maps::dijkstra;
        using robofat2::maps::pos;
        using robofat2::maps::node;

        class hill_dijkstra {
        public:
            explicit hill_dijkstra(hill_map &map,
                                   int heightWeight,
                                   int travelWeight,
                                   int unknownWeight);

            std::deque<pos> operator()(const pos &start);

        private:
            dijkstra_meta::distance_t sensor_fn(const node &start, const node &end);

            static bool finish_fn(const pos &where);

            hill_map *m_map;
            int m_heightWeight;
            int m_travelWeight;
            int m_unknownWeight;
        };
    }
}

#endif //DUCTTAPE_HILL_DIJKSTRA_HPP
