#include <cassert>
#include <utility>

#include "hill_dijkstra.hpp"

using namespace robofat2::maps;
using namespace ducttape::maps;

using std::placeholders::_1;
using std::placeholders::_2;

hill_dijkstra::hill_dijkstra(hill_map &map,
                             int heightWeight,
                             int travelWeight,
                             int unknownWeight)
        : m_map(&map),
          m_heightWeight(heightWeight),
          m_travelWeight(travelWeight),
          m_unknownWeight(unknownWeight) {}

std::deque<pos> hill_dijkstra::operator()(const pos &start) {
    dijkstra search(start,
                    std::bind(&hill_dijkstra::sensor_fn, this, _1, _2),
                    &hill_dijkstra::finish_fn);
    return search();
}

dijkstra_meta::distance_t
hill_dijkstra::sensor_fn(const node &startNode,
                         const node &endNode) {
    const pos &startPos = startNode.coord;
    const pos &endPos = endNode.coord;

    if (!m_map->isIndexValid(startPos) ||
        !m_map->isIndexValid(endPos)) {
        return dijkstra_meta::nonexistent_edge;
    }
    tile start = m_map->get(startPos);
    tile end = m_map->get(endPos);

    assert(start.is_real());
    assert(end.is_real());

    // proven to be unreachable
    if (!is_reachable(start, end)) {
        return dijkstra_meta::nonexistent_edge;
    }

    // else just do some random math and hope that it succeeds
    int endUncertainty = end.get_upper_bound() - end.get_lower_bound();
    int lowerBoundDifference = end.get_lower_bound() - start.get_lower_bound();

    return m_travelWeight +
           m_unknownWeight * endUncertainty +
           m_heightWeight * std::abs(lowerBoundDifference);
}

bool hill_dijkstra::finish_fn(const pos &where) {
    return where.y() == 0;
}
