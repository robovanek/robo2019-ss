#include <hill_map.hpp>
#include <sstream>

using namespace ducttape::maps;

hill_map::hill_map()
        : tile_map<tile>(6, 9, tile(true), tile(false)) {
    drawInitial();
}

void hill_map::drawInitial() {
    for (int x = 0; x < (int) width(); x++) {
        set(pos(x, 0), tile(true, tile::height_floor));
        set(pos(x, 8), tile(true, tile::height_floor));
    }
}

tile::tile()
        : tile(true){}

tile::tile(bool real)
        : tile(real, height_min, height_max) {}

tile::tile(bool real, int height)
        : tile(real, height, height) {}

tile::tile(bool real, int lowerBound, int upperBound)
        : m_heightMin(lowerBound), m_heightMax(upperBound), m_real(real) {}

int tile::get_lower_bound() const {
    return m_heightMin;
}

int tile::get_upper_bound() const {
    return m_heightMax;
}

bool tile::is_real() const {
    return m_real;
}

tile tile::with_lower_bound(int value) {
    int max = std::max(m_heightMax, value);
    return tile(m_real, value, max);
}

tile tile::with_upper_bound(int value) {
    int min = std::min(m_heightMin, value);
    return tile(m_real, min, value);
}

tile tile::with_height(int value) {
    return tile(m_real, value);
}

std::string tile::as_string() const {
    std::ostringstream ss;
    ss << "tile<" << m_heightMin << "; " << m_heightMax << ">";
    return ss.str();
}

bool ducttape::maps::is_reachable(tile start, tile end) {
    // detect best possible step
    int hStartLo = start.get_lower_bound();
    int hStartHi = start.get_upper_bound();
    int hEndLo = end.get_lower_bound();
    int hEndHi = end.get_upper_bound();

    int bestStepUp = hEndLo - hStartHi;
    int bestStepDown = hStartLo - hEndHi;

    // if one of these does not hold, the node is proven to be unreachable
    return bestStepUp <= 1 && bestStepDown <= 1;
}
