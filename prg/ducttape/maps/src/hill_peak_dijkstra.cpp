
#include <robofat2/utilities/numbers.hpp>
#include <cassert>

#include "hill_peak_dijkstra.hpp"

using namespace ducttape::maps;
using namespace robofat2::maps;
using namespace robofat2::utilities;

using std::placeholders::_1;
using std::placeholders::_2;

hill_peak_dijkstra::hill_peak_dijkstra(hill_map &map,
                                       float heightWeight,
                                       float travelWeight,
                                       float unknownWeight)
        : m_map(&map),
          m_heightWeight(heightWeight),
          m_travelWeight(travelWeight),
          m_unknownWeight(unknownWeight) {}

std::deque<pos>
hill_peak_dijkstra::operator()(const pos &start) {
    dijkstra search(start,
                    std::bind(&hill_peak_dijkstra::sensor_fn, this, _1, _2),
                    std::bind(&hill_peak_dijkstra::finish_fn, this, _1));
    return search();
}

dijkstra_meta::distance_t
hill_peak_dijkstra::sensor_fn(const node &startNode,
                              const node &endNode) {
    const pos &startPos = startNode.coord;
    const pos &endPos = endNode.coord;

    if (!m_map->isIndexValid(startPos) ||
        !m_map->isIndexValid(endPos)) {
        return dijkstra_meta::nonexistent_edge;
    }
    tile start = m_map->get(startPos);
    tile end = m_map->get(endPos);

    assert(start.is_real());
    assert(end.is_real());

    // proven to be unreachable
    if (!is_reachable(start, end)) {
        return dijkstra_meta::nonexistent_edge;
    }
    // else just do some random math and hope that it succeeds
    int endCertainty = tile::height_max - (end.get_upper_bound() - end.get_lower_bound());
    int lowerBoundLowness = tile::height_max - end.get_lower_bound();

    return roundToInt(m_travelWeight +
                      m_unknownWeight * endCertainty +
                      m_heightWeight * std::abs(lowerBoundLowness));
}

bool
hill_peak_dijkstra::finish_fn(const pos &where) {
    tile t = m_map->get(where);
    return t.get_lower_bound() != t.get_upper_bound();
}
