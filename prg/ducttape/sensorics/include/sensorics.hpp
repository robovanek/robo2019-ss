#ifndef DUCTTAPE_SENSORICS_HPP
#define DUCTTAPE_SENSORICS_HPP

#include <chrono>
#include <string>
#include <robofat2/threading/task.hpp>
#include <robofat2/maps/orientation.hpp>
#include <robofat2/settings/settings.hpp>
#include <robofat2/logging/sink.hpp>
#include <robofat2/logging/source.hpp>
#include <ev3dev.h>

namespace ducttape {
    enum class sonic_result {
        indeterminate = -1,
        no_cliff = 0,
        cliff_down_1 = 1,
        cliff_down_2 = 2,
        cliff_down_3 = 3,
        cliff_down_4 = 4,
        cliff_down_5 = 5,
        cliff_down_6 = 6,
        cliff_down_disaster = 1000
    };

    std::string sonic_as_string(sonic_result input);

    template<typename T>
    class filtered_value {
    public:
        using clock = std::chrono::steady_clock;

    public:
        explicit
        filtered_value(clock::duration minAge)
                : m_filtered(),
                  m_last(),
                  m_lastChange(clock::now()),
                  m_minAge(minAge),
                  m_filterPassed(false) {}

        void update(T now) {
            if (m_last == now && !m_filterPassed) {
                clock::duration delta = clock::now() - m_lastChange;

                if (delta > m_minAge) {
                    m_filtered = now;
                    m_filterPassed = true;
                }
            } else {
                m_last = now;
                m_lastChange = clock::now();
                m_filterPassed = false;
            }
        }

        T filtered() const {
            return m_filtered;
        }

    private:
        T m_filtered;
        T m_last;
        clock::time_point m_lastChange;
        clock::duration m_minAge;
        bool m_filterPassed;
    };

    class sensorics_base : public robofat2::threading::task {
    public:
        sensorics_base();

        virtual bool single_wall_detected() = 0;

        virtual bool high_wall_detected() = 0;

        virtual sonic_result analyze_sonic() = 0;

        virtual int gyro_azimuth() = 0;

        virtual void gyro_calibrate() = 0;

        virtual void gyro_align(robofat2::maps::orient currentDirection) = 0;

        virtual int rawGyro() = 0;

        virtual int rawSonic() = 0;

        virtual int rawColor() = 0;

        virtual bool rawTouch() = 0;

    protected:
        void update() override = 0;
    };

    class sensorics : public sensorics_base {
    public:
        sensorics(robofat2::settings::settings &conf,
                  std::shared_ptr<robofat2::logging::sink> logDevice);

        bool single_wall_detected() final;

        bool high_wall_detected() final;

        sonic_result analyze_sonic() final;

        int gyro_azimuth() final;

        void gyro_calibrate() final;

        void gyro_align(robofat2::maps::orient currentDirection) final;

        int rawGyro() override;

        int rawSonic() override;

        int rawColor() override;

        bool rawTouch() override;

    protected:
        void update() final;

    private:
        sonic_result analyze_sonic_real();

        ev3dev::gyro_sensor m_sensorGyro;
        ev3dev::ultrasonic_sensor m_sensorSonic;
        ev3dev::color_sensor m_sensorColor;
        ev3dev::touch_sensor m_sensorTouch;

        filtered_value<bool> m_singleWallDetected;
        int m_lightThreshold;

        filtered_value<bool> m_highWallDetected;

        filtered_value<sonic_result> m_sonicResult;
        int m_sonicWindow;
        std::array<int, 7> m_sonicCenters;

        float m_gyroFactor;
        int m_sonicDisaster;
        int m_gyroAzimuth;
        int m_gyroZeroState;

        robofat2::logging::source m_log;
    };
}

#endif //DUCTTAPE_SENSORICS_HPP
