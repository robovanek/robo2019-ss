#include "sensorics.hpp"

#include <utility>
#include <robofat2/utilities/numbers.hpp>
#include <robofat2/maps/heading.hpp>
#include <thread>

using namespace ducttape;
using namespace robofat2::utilities;
using namespace robofat2::maps;
using std::chrono::nanoseconds;


sensorics_base::sensorics_base()
        : robofat2::threading::task("sensor data analysis") {}

sensorics::sensorics(robofat2::settings::settings &conf,
                     std::shared_ptr<robofat2::logging::sink> logDevice) :
        m_sensorGyro(),
        m_sensorSonic(),
        m_sensorColor(),
        m_sensorTouch(),
        m_singleWallDetected(
                conf.get_duration<nanoseconds>("sensor/color/stable_time")),
        m_lightThreshold(
                conf.get_integer("sensor/color/threshold")),
        m_highWallDetected(
                conf.get_duration<nanoseconds>("sensor/touch/stable_time")),
        m_sonicResult(
                conf.get_duration<nanoseconds>("sensor/sonic/stable_time")),
        m_sonicWindow(
                conf.get_integer("sensor/sonic/window")),
        m_sonicCenters(
                {
                        conf.get_integer("sensor/sonic/center_0"),
                        conf.get_integer("sensor/sonic/center_1"),
                        conf.get_integer("sensor/sonic/center_2"),
                        conf.get_integer("sensor/sonic/center_3"),
                        conf.get_integer("sensor/sonic/center_4"),
                        conf.get_integer("sensor/sonic/center_5"),
                        conf.get_integer("sensor/sonic/center_6"),
                }),
        m_gyroFactor(
                conf.get_float("sensor/gyro/factor")),
        m_sonicDisaster(
                conf.get_integer("sensor/sonic/disaster_threshold")),
        m_gyroAzimuth(0),
        m_gyroZeroState(0),
        m_log("sense", std::move(logDevice)) {

    m_log.info("initializing gyro...");
    m_sensorGyro.set_mode(ev3dev::gyro_sensor::mode_gyro_ang);

    m_log.info("initializing sonic...");
    m_sensorSonic.set_mode(ev3dev::ultrasonic_sensor::mode_us_dist_cm);

    m_log.info("initializing color...");
    m_sensorColor.set_mode(ev3dev::color_sensor::mode_col_reflect);

    m_log.info("initializing touch...");
    m_sensorTouch.set_mode(ev3dev::touch_sensor::mode_touch);
}

using dt_clock = sensorics::clock;

void sensorics::update() {
    // gyro    is         clockwise-positive
    // program is counter-clockwise-positive
    // => negate gyro value
    // + shift the value to report current "alignment" offset
    m_gyroAzimuth = roundToInt(-rawGyro() * m_gyroFactor - m_gyroZeroState);
    m_highWallDetected.update(
            rawTouch()
    );
    m_singleWallDetected.update(
            rawColor() > m_lightThreshold
    );
    m_sonicResult.update(
            analyze_sonic_real()
    );
}

void sensorics::gyro_calibrate() {
    m_sensorGyro.set_mode(ev3dev::gyro_sensor::mode_gyro_rate);
    std::this_thread::sleep_for(std::chrono::milliseconds(250));

    m_sensorGyro.set_mode(ev3dev::gyro_sensor::mode_gyro_ang);
    std::this_thread::sleep_for(std::chrono::milliseconds(250));
}

int sensorics::gyro_azimuth() {
    return m_gyroAzimuth;
}

sonic_result sensorics::analyze_sonic() {
    return m_sonicResult.filtered();
}

bool sensorics::high_wall_detected() {
    return m_highWallDetected.filtered();
}

bool sensorics::single_wall_detected() {
    return m_singleWallDetected.filtered();
}

sonic_result sensorics::analyze_sonic_real() {
    int real = rawSonic();

    if (real == 2550) {
        // probably very close
        return sonic_result::indeterminate;
    }

    for (int i = 0; i <= 6; i++) {
        int measurement_center = m_sonicCenters.at(i);
        int window_min = measurement_center - m_sonicWindow;
        int window_max = measurement_center + m_sonicWindow;

        if (is_bounded(real, window_min, window_max)) {
            return (sonic_result) i;
        }
    }
    if (real >= m_sonicDisaster) {
        return sonic_result::cliff_down_disaster;
    }
    return sonic_result::indeterminate;
}

void sensorics::gyro_align(orient currentDirection) {
    heading normalizedHeading(currentDirection);
    //  +-- this is an additional correction - the old one won't be erased
    //  |   (this would throw the angle off, as the readings are already relative/corrected)
    //  |                        +-- this will be subtracted (i.e. it would report zero next time)
    //  |                        |                +-- however this will be added in the end
    //  v                        v                v   (so the new reported value will be equal to this)
    int additionalSubtraction = gyro_azimuth() - normalizedHeading.angle;
    m_gyroZeroState += additionalSubtraction;
}

int sensorics::rawGyro() {
    return m_sensorGyro.angle(false);
}

int sensorics::rawSonic() {
    return roundToInt(m_sensorSonic.distance_centimeters(false) * 10.0f);
}

int sensorics::rawColor() {
    return m_sensorColor.reflected_light_intensity(false);
}

bool sensorics::rawTouch() {
    return m_sensorTouch.is_pressed(false);
}

std::string sonic_as_string(sonic_result input) {
    switch (input) {
        case sonic_result::indeterminate:
            return "?";
        case sonic_result::no_cliff:
            return "0";
        case sonic_result::cliff_down_1:
            return "1";
        case sonic_result::cliff_down_2:
            return "2";
        case sonic_result::cliff_down_3:
            return "3";
        case sonic_result::cliff_down_4:
            return "4";
        case sonic_result::cliff_down_5:
            return "5";
        case sonic_result::cliff_down_6:
            return "6";
        case sonic_result::cliff_down_disaster:
            return "∞";
        default:
            return "fuj";
    }
}
