#include <thread>
#include "arm.hpp"

using std::chrono::milliseconds;
using namespace ducttape;
using namespace ev3dev;

static std::unordered_map<arm_action, std::string> action_names = {
        {arm_action::revert_to_normal,     "arm/action/revert"},
        {arm_action::prepare_for_pushdown, "arm/action/prepare"},
        {arm_action::pushdown,             "arm/action/push"},
        {arm_action::fall_balancer,        "arm/action/balancer"},
        {arm_action::fail,                 "arm/action/fail"},
};

arm_controller::arm_controller(robofat2::settings::settings &conf,
                               std::shared_ptr<robofat2::logging::sink> log)
        : m_armMotor(),
          m_topAngle(0),
          m_bottomAngle(0),
          m_log("arm", log) {

    for (auto &&action : action_names) {
        action_info info = {
                conf.get_float(action.second + "/percent"),
                conf.get_integer(action.second + "/speed")
        };
        m_actions.emplace(action.first, std::move(info));
    }

    initialize_motor(conf);
}

void arm_controller::initialize_motor(robofat2::settings::settings &conf) {
    m_armMotor.reset();
    m_armMotor.set_polarity(motor::polarity_normal);
    m_armMotor.set_stop_action(motor::stop_action_hold);

    m_armMotor.set_ramp_up_sp(conf.get_integer("arm/ramp/time_up"));
    m_armMotor.set_ramp_down_sp(conf.get_integer("arm/ramp/time_down"));

    m_armMotor.set_position_p(conf.get_integer("arm/pid/hold/kp"));
    m_armMotor.set_position_i(conf.get_integer("arm/pid/hold/ki"));
    m_armMotor.set_position_d(conf.get_integer("arm/pid/hold/kd"));
    m_armMotor.set_speed_p(conf.get_integer("arm/pid/speed/kp"));
    m_armMotor.set_speed_i(conf.get_integer("arm/pid/speed/ki"));
    m_armMotor.set_speed_d(conf.get_integer("arm/pid/speed/kd"));
}

void arm_controller::calibrate() {
    calibrate_step(m_bottomAngle, 720);
    calibrate_step(m_topAngle, -720);
    perform(arm_action::revert_to_normal);
}

void arm_controller::calibrate_step(int &finalTacho, int speed) {
    m_log.info("starting calibration, speed = %d °/s", speed);

    m_armMotor.set_speed_sp(speed);
    m_armMotor.run_forever();

    while (!m_armMotor.state().count(motor::state_stalled)) {
        std::this_thread::sleep_for(milliseconds(10));
    }
    m_armMotor.stop();
    std::this_thread::sleep_for(milliseconds(200));
    finalTacho = m_armMotor.position();

    m_log.info("calibrated to %d°", finalTacho);
}

void arm_controller::perform(arm_action action) {
    perform(m_actions.at(action));
}

void arm_controller::perform(const action_info &info) {
    float tacho = (0.0f + info.fraction_top / 100.0f) * (float) m_topAngle +
                  (1.0f - info.fraction_top / 100.0f) * (float) m_bottomAngle;

    m_armMotor.set_speed_sp(info.speed);
    m_armMotor.set_position_sp((int) tacho);
    m_armMotor.run_to_abs_pos();
}

void arm_controller::stop() {
    m_armMotor.stop();
}

arm_controller::~arm_controller() {
    m_armMotor.reset();
}

bool arm_controller::moving() {
    auto state = m_armMotor.state();
    return state.count(motor::state_running) && !state.count(motor::state_stalled);
}
