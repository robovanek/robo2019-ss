#include <robofat2/maps/heading.hpp>
#include "turn_regulator_base.hpp"

using namespace ducttape;
using namespace robofat2::maps;

turn_regulator_base::turn_regulator_base(movement_model &model,
                                         motor_pair &motors,
                                         sensorics_base &sensors)
        : task("turn regulator"),
          m_model(&model), m_motors(&motors), m_sensors(&sensors),
          m_targetDirection(orient::north),
          m_targetHeading(m_targetDirection),
          m_setSpeeds(0, 0),
          m_running(false) {}

void turn_regulator_base::start(orient target, angular_speed_pair<int> speed) {
    m_targetDirection = target;
    m_targetHeading = heading(target);
    m_setSpeeds = speed;
    mark_running();
    do_start();
}

void turn_regulator_base::update() {
    if (is_running()) {
        do_run();
        if (!is_running()) {
            do_stop();
        }
    }
}

bool turn_regulator_base::is_running() const {
    return m_running;
}

void turn_regulator_base::mark_running() {
    m_running = true;
}

void turn_regulator_base::mark_finished() {
    m_running = false;
}
