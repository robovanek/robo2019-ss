
#include <mechanics.hpp>
#include <turn_regulator_stepwise.hpp>
#include <turn_regulator_continuous.hpp>

using namespace ducttape;
using namespace robofat2::utilities;
using namespace robofat2::settings;
using namespace robofat2::logging;

mechanics::mechanics(robofat2::settings::settings &conf,
                     sensorics_base &sensorics,
                     spatial::environment &space,
                     const std::shared_ptr<robofat2::logging::sink> &log) :
        m_motors("ev3-ports:outA", "ev3-ports:outD"),
        m_model(conf),
        m_sensors(&sensorics),
        m_space(&space),
        m_straight(conf, m_motors, sensorics, space, *this, log),
        m_turn(initializeTurnReg(conf, m_model, m_motors, sensorics, log)) {
    m_motors.initialize_motors(conf.get_integer("mechanics/ramp/time_up"),
                               conf.get_integer("mechanics/ramp/time_down"),
                               conf.get_integer("mechanics/pid/hold/kp"),
                               conf.get_integer("mechanics/pid/hold/ki"),
                               conf.get_integer("mechanics/pid/hold/kd"),
                               conf.get_integer("mechanics/pid/speed/kp"),
                               conf.get_integer("mechanics/pid/speed/ki"),
                               conf.get_integer("mechanics/pid/speed/kd"));
}

void mechanics::start_unlimited_line(speed_type type,
                                     bool regulate) {
    m_startSpeed = m_model.get_angular_speed_pair(type);
    store_start_state();
    m_motors.start_unlimited(m_startSpeed);
    if (regulate) {
        m_straight.enable(m_startSpeed, false);
    } else {
        m_straight.disable();
    }
}

void mechanics::start_limited_line(distance_type distance,
                                   speed_type speed,
                                   bool regulate) {
    return start_limited_line(distance, speed, 1, regulate);
}

void mechanics::start_limited_line(distance_type distance,
                                   speed_type speed,
                                   int amount,
                                   bool regulate) {
    auto deltas = m_model.calculate_tacho_for_travel(distance) * amount;
    start_limited(deltas, speed, regulate);
}

void mechanics::start_limited_turn(orient targetDirection,
                                   speed_type speed) {
    m_startSpeed = m_model.get_angular_speed_pair(speed);
    m_straight.disable();
    store_start_state();
    m_turn->start(targetDirection, m_startSpeed);
}


void mechanics::start_limited(angular_pos_pair<int> deltas,
                              speed_type speed,
                              bool regulate) {
    m_startSpeed = m_model.get_angular_speed_pair(speed);
    auto tachos = m_motors.get_current_position() + deltas;

    store_start_state();
    m_motors.start_limited(m_startSpeed, tachos);

    if (regulate && deltas.left == deltas.right) {
        m_straight.enable(m_startSpeed, true);
    } else {
        m_straight.disable();
    }
}

void mechanics::stop() {
    m_motors.stop();
    m_straight.disable();
}

bool mechanics::moving() {
    return motors_rotating() || fixup_running() || m_turn->is_running();
}

bool mechanics::motors_rotating() {
    return m_motors.moving();
}

bool mechanics::fixup_running() {
    return m_straight.fuckup_fixup_running();
}


float mechanics::distance_travelled_since_start() {
    if (fixup_running()) {
        return 0.0f;
    } else {
        return m_model.calculate_travelled_distance(get_wheel_deltas());
    }
}

float mechanics::angle_turned_since_start() {
    if (fixup_running()) {
        return 0.0f;
    } else {
        return m_model.calculate_rotated_angle(get_wheel_deltas());
    }
}

angular_pos_pair<int> mechanics::get_wheel_deltas() {
    return m_motors.get_current_position() - m_startTacho;
}

void mechanics::store_start_state() {
    m_startTacho = m_motors.get_current_position();
    m_startTimer.set_mark_to_now();
}

std::chrono::milliseconds mechanics::time_elapsed_since_start() {
    using namespace std::chrono;

    if (fixup_running()) {
        return milliseconds(0);
    } else {
        return duration_cast<milliseconds>(m_startTimer.get_elapsed_since_mark());
    }
}

bool mechanics::has_travelled_since_start(distance_type type) {
    if (fixup_running()) {
        return false;
    } else {
        return distance_travelled_since_start() >= m_model.get_linear_distance(type);
    }
}

straight_regulator &mechanics::straight_reg() {
    return m_straight;
}

turn_regulator_base &mechanics::turn_reg() {
    return *m_turn;
}

turn_regulator_base *mechanics::initializeTurnReg(settings &conf,
                                                  movement_model &model,
                                                  motor_pair &motors,
                                                  sensorics_base &sensors,
                                                  std::shared_ptr<sink> log) {
    if (conf.get_bool("mechanics/gyro/turn/use_pid")) {
        return new turn_regulator_continuous(conf, model, motors, sensors);
    } else {
        return new turn_regulator_stepwise(conf, model, motors, sensors, log);
    }
}
