#include "movement_model.hpp"

using namespace ducttape;

static std::unordered_map<speed_type, std::string> speed_names = {
        {speed_type::free_travel,   "mechanics/speed/free_travel"},
        {speed_type::travelback,    "mechanics/speed/travelback"},
        {speed_type::climbup_lower, "mechanics/speed/climb_up_lower"},
        {speed_type::climbup_upper, "mechanics/speed/climb_up_upper"},
        {speed_type::climbdown,     "mechanics/speed/climb_down"},
        {speed_type::turn,          "mechanics/speed/turning"},
        {speed_type::fixup,         "mechanics/speed/fixup"},
        {speed_type::kamikaze,      "mechanics/speed/kamikaze"},
        {speed_type::fail,          "mechanics/speed/fail"},
};

static std::unordered_map<distance_type, std::string> distance_names = {
        {distance_type::travelback_too_deep,  "mechanics/distance/travelback/deep"},
        {distance_type::travelback_too_high,  "mechanics/distance/travelback/high"},
        {distance_type::travelback_single,    "mechanics/distance/travelback/single"},
        {distance_type::initial_travel,       "mechanics/distance/initial"},
        {distance_type::climb_lower_pregyro,  "mechanics/distance/climb/lower/pregyro"},
        {distance_type::climb_lower_postgyro, "mechanics/distance/climb/lower/postgyro"},
        {distance_type::climb_upper,          "mechanics/distance/climb/upper"},
        {distance_type::after_climb_travel,   "mechanics/distance/climb/after"},
        {distance_type::fall_upper,           "mechanics/distance/fall/upper"},
        {distance_type::fall_lower,           "mechanics/distance/fall/lower"},
        {distance_type::after_fall_travel,    "mechanics/distance/fall/after"},
        {distance_type::block,                "mechanics/distance/block"},
        {distance_type::fixup,                "mechanics/distance/fixup"},
        {distance_type::fail,                 "mechanics/distance/fail"},
        {distance_type::kamikaze,             "mechanics/distance/kamikaze"},
};

ducttape::movement_model::movement_model(robofat2::settings::settings &conf)
        : robofat2::diffbot::movement_model(conf.get_float("dimensions/track_width"),
                                            conf.get_float("dimensions/wheel_radius")),
          m_speeds(speed_names.size()),
          m_distances(distance_names.size()) {

    for (auto &&pair : speed_names)
        m_speeds.emplace(pair.first, conf.get_float(pair.second));

    for (auto &&pair : distance_names)
        m_distances.emplace(pair.first, conf.get_float(pair.second));
}

angular_pos_pair<int> ducttape::movement_model::calculate_tacho_for_travel(distance_type type) const {
    float distance = get_linear_distance(type);
    return robofat2::diffbot::movement_model::calculate_tacho_for_travel(distance);
}

float ducttape::movement_model::get_linear_speed(speed_type type) const {
    return m_speeds.at(type);
}

float ducttape::movement_model::get_linear_distance(distance_type type) const {
    return m_distances.at(type);
}

angular_speed_pair<int> ducttape::movement_model::get_angular_speed_pair(speed_type type) const {
    float speed = get_linear_speed(type);
    return robofat2::diffbot::movement_model::get_angular_speed_pair(speed);
}
