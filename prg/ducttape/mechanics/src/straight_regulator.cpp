#include <robofat2/utilities/numbers.hpp>
#include "straight_regulator.hpp"

using namespace robofat2::settings;
using namespace robofat2::utilities;
using namespace robofat2::diffbot;
using namespace robofat2::maps;
using namespace ducttape;

straight_regulator::straight_regulator(settings &conf,
                                       motor_pair &motors,
                                       sensorics_base &sensors,
                                       spatial::environment &space,
                                       mechanics &mech,
                                       const std::shared_ptr<robofat2::logging::sink> &logDevice)
        : task("straight move regulator"),
          m_mech(&mech),
          m_motors(&motors),
          m_sensors(&sensors),
          m_space(&space),
          m_fixup(mech, logDevice),
          m_fuckupThreshold(
                  conf.get_integer("mechanics/gyro/fuckup/threshold")),
          m_gyroStraightPid(
                  conf.get_float("mechanics/gyro/kp"),
                  conf.get_float("mechanics/gyro/ki"),
                  conf.get_float("mechanics/gyro/kd"),
                  conf.get_float("mechanics/gyro/i_forget"),
                  conf.get_float("mechanics/gyro/i_max"),
                  conf.get_float("mechanics/gyro/dfilt_now")
          ),
          m_gyroMaxCorrection(
                  conf.get_float("mechanics/gyro/max_corr_total")),
          m_gyroIntegralFreeze(
                  conf.get_float("mechanics/gyro/max_corr_i_freeze")),
          m_canBeEnabled(
                  conf.get_bool("mechanics/gyro/enable")),
          m_log("straight", logDevice) {
    task::set_run_period(std::chrono::milliseconds(100));
}

void straight_regulator::enable(angular_speed_pair<int> baseSpeeds, bool limited) {
    if (m_canBeEnabled) {
        m_active = true;
        m_limited = limited;
        m_baseSpeed = baseSpeeds;
        m_started = false;
    }
}

void straight_regulator::update() {
    if (m_fixup.running()) {
        return m_fixup.update();
    } else if (m_active) {
        return gyro_update();
    }
}

void straight_regulator::gyro_update() {
    if (!m_started) {
        m_gyroStraightPid.reset();
        m_started = true;
        return;
    }
    if (!m_motors->moving()) {
        m_active = false;
        return;
    }

    int fix = get_gyro_fix();
    if (std::abs(fix) >= m_fuckupThreshold) {
        m_log.error("FIXUP STARTED!");
        return m_fixup.start(m_space->get_current_direction(), m_limited);
    }
    float correction = get_correction(fix);
    push(correction);
}

int straight_regulator::get_gyro_fix() {
    auto currentGyro = m_sensors->gyro_azimuth();
    auto currentDirection = m_space->get_current_direction();

    heading current(currentGyro);
    heading expect(currentDirection);
    return current.shortestPathTo(expect);
}

void straight_regulator::push(float correction) {
    angular_speed_pair<int> speeds = {
            m_baseSpeed.left - roundToInt(correction),
            m_baseSpeed.right + roundToInt(correction)
    };

    if (m_limited) {
        m_motors->start_limited(speeds, m_motors->get_target_position());
    } else {
        m_motors->start_unlimited(speeds);
    }
}

float straight_regulator::get_correction(int gyroFix) {
    float correction = m_gyroStraightPid.process((float) gyroFix);
    correction = limit(correction,
                       -m_gyroMaxCorrection,
                       +m_gyroMaxCorrection);
    m_gyroStraightPid.setIntegralFreeze(std::abs(correction) > m_gyroIntegralFreeze);
    return correction;
}

void straight_regulator::disable() {
    m_active = false;
}

bool straight_regulator::fuckup_fixup_running() const {
    return m_fixup.running();
}
