#include <robofat2/maps/heading.hpp>
#include <robofat2/utilities/numbers.hpp>

#include "turn_regulator_continuous.hpp"

using namespace ducttape;
using namespace robofat2::maps;
using namespace robofat2::utilities;


turn_regulator_continuous::turn_regulator_continuous(robofat2::settings::settings &conf,
                                                     movement_model &model,
                                                     motor_pair &motors,
                                                     sensorics_base &sensors)
        : turn_regulator_base(model, motors, sensors),
          m_pid(conf.get_float("mechanics/gyro/turn/pid/kp"),
                conf.get_float("mechanics/gyro/turn/pid/ki"),
                conf.get_float("mechanics/gyro/turn/pid/kd"),
                conf.get_float("mechanics/gyro/turn/pid/i_forget"),
                conf.get_float("mechanics/gyro/turn/pid/i_max"),
                conf.get_float("mechanics/gyro/turn/pid/dfilt_now")),
          m_speedLimit(0),
          m_proportionalThreshold(
                  conf.get_float("mechanics/gyro/turn/pid/threshold/p")),
          m_integralThreshold(
                  conf.get_float("mechanics/gyro/turn/pid/threshold/i")),
          m_derivativeThreshold(
                  conf.get_float("mechanics/gyro/turn/pid/threshold/d")) {
    this->set_run_period(std::chrono::milliseconds(100));
}

void turn_regulator_continuous::do_start() {
    m_pid.reset();
    m_pid.setIntegralFreeze(true);

    m_speedLimit = m_setSpeeds.average();

    heading currentHeading(m_sensors->gyro_azimuth());
    int sign = signum(currentHeading.shortestPathTo(m_targetHeading));

    angular_speed_pair<int> speeds = {-sign * m_speedLimit, +sign * m_speedLimit};
    m_motors->start_unlimited(speeds);
}

void turn_regulator_continuous::do_run() {
    heading currentHeading(m_sensors->gyro_azimuth());
    int input = currentHeading.shortestPathTo(m_targetHeading);

    float fRawOutput = m_pid.process((float) input);
    int iRawOutput = roundToInt(fRawOutput);
    int iLimOutput = limit(iRawOutput, -m_speedLimit, +m_speedLimit);

    bool overloaded = iRawOutput != iLimOutput;
    m_pid.setIntegralFreeze(overloaded);

    if (isFinished(fRawOutput)) {
        mark_finished();
        return;
    }

    angular_speed_pair<int> speeds = {-iLimOutput, +iLimOutput};
    m_motors->start_unlimited(speeds);
}

void turn_regulator_continuous::do_stop() {
    m_motors->stop();
}

bool turn_regulator_continuous::isFinished(float rawControl) const {
    return m_proportionalThreshold > std::abs(rawControl);
}
