#include "turn_regulator_stepwise.hpp"

#include <utility>
#include <robofat2/maps/heading.hpp>

using namespace ducttape;
using namespace robofat2::maps;

turn_regulator_stepwise::turn_regulator_stepwise(robofat2::settings::settings &conf,
                                                 movement_model &model,
                                                 motor_pair &motors,
                                                 sensorics_base &sensors,
                                                 std::shared_ptr<robofat2::logging::sink> logDevice)
        : turn_regulator_base(model, motors, sensors),
          m_threshold(conf.get_integer("mechanics/gyro/turn/threshold")),
          m_log("turn", std::move(logDevice)) {
    set_run_period(std::chrono::milliseconds(100));
}

void turn_regulator_stepwise::do_start() {
    do_turn();
}

void turn_regulator_stepwise::do_run() {
    if (!m_motors->moving()) {
        int fix = get_fix_angle();
        if (std::abs(fix) > m_threshold) {
            do_turn(fix);
        } else {
            mark_finished();
        }
    }
}

void turn_regulator_stepwise::do_stop() {

}

int turn_regulator_stepwise::get_fix_angle() {
    heading sourceHeading(m_sensors->gyro_azimuth());

    return sourceHeading.shortestPathTo(m_targetHeading);
}

void turn_regulator_stepwise::do_turn() {
    return do_turn(get_fix_angle());
}

void turn_regulator_stepwise::do_turn(int fix) {
    m_log.debug("turning: %d °", fix);
    m_log.debug("target:  %d °", m_targetHeading.angle);
    m_log.debug("current: %d °", m_sensors->gyro_azimuth());
    auto deltas = m_model->calculate_tacho_for_rotation(fix);
    auto tachos = m_motors->get_current_position() + deltas;
    m_motors->start_limited(m_setSpeeds, tachos);
}
