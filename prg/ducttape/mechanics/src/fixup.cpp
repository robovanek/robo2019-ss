#include "fixup.hpp"

#include <utility>
#include "mechanics.hpp"

using namespace ducttape;
using namespace robofat2::logging;

fixup_capture::fixup_capture() = default;

template<typename DurT, typename ClockT>
static long unix_ts(std::chrono::time_point<ClockT, DurT> point) {
    auto seconds = std::chrono::duration_cast<std::chrono::seconds>(point.time_since_epoch());
    return seconds.count();
}

fixup_capture::fixup_capture(mechanics &mech, bool wasLimited, source log)
        : m_targetTacho(mech.m_motors.get_target_position()),
          m_startTacho(mech.m_startTacho),
          m_startTime(mech.m_startTimer.get_mark()),
          m_captureTacho(mech.m_motors.get_current_position()),
          m_captureTime(std::chrono::steady_clock::now()),
          m_speed(mech.m_startSpeed),
          m_wasLimited(wasLimited) {
    log.warn("capturing movement state");
    log.warn("start   tacho : %s °", m_startTacho.as_string().c_str());
    log.warn("target  tacho : %s °", m_targetTacho.as_string().c_str());
    log.warn("capture tacho : %s °", m_captureTacho.as_string().c_str());
    log.warn("start   time  : %ld s", unix_ts(m_startTime));
    log.warn("capture time  : %ld s", unix_ts(m_captureTime));
    log.warn("speed         : %s °/s", m_speed.as_string().c_str());
    log.warn("move type     : %s", m_wasLimited ? "limited" : "unlimited");
}

void fixup_capture::restore(mechanics &mech, source log) {
    auto restoreTacho = mech.m_motors.get_current_position();
    auto restoreTime = clock::now();

    int completed = getCompletedDegrees(restoreTacho);
    int remaining = getRemainingDegrees(completed);

    auto newStartTacho = restoreTacho - angular_pos_pair<int>{completed};
    auto newTargetTacho = restoreTacho + angular_pos_pair<int>{remaining};
    auto newStartTime = m_startTime + (restoreTime - m_captureTime);

    mech.m_startTacho = newStartTacho;
    mech.m_startTimer.set_mark(newStartTime);

    log.warn("restoring movement state");
    log.warn("restore    tacho : %s °", restoreTacho.as_string().c_str());
    log.warn("restore    time  : %ld s", unix_ts(restoreTime));
    log.warn("new start  tacho : %s °", newStartTacho.as_string().c_str());
    log.warn("new target tacho : %s °", newTargetTacho.as_string().c_str());
    log.warn("new start  time  : %ld s", unix_ts(newStartTime));

    if (m_wasLimited) {
        mech.m_motors.start_limited(m_speed, newTargetTacho);
    } else {
        mech.m_motors.start_unlimited(m_speed);
    }
    mech.m_straight.enable(m_speed, m_wasLimited);
}

int fixup_capture::getCompletedDegrees(angular_pos_pair<int> restoreTacho) const {
    int failApprox = (m_captureTacho - restoreTacho).average();
    int completedWithFail = (m_captureTacho - m_startTacho).average();
    return completedWithFail - failApprox;
}

int fixup_capture::getRemainingDegrees(int completed) const {
    int whole = (m_targetTacho - m_startTacho).average();

    return whole - completed;
}

fixup::fixup(mechanics &mech, std::shared_ptr<sink> logDevice) :
        m_mech(&mech),
        m_log("fixup", std::move(logDevice)) {}

void fixup::start(orient target, bool wasLimited) {
    // step 1:
    m_state = cycle(target, *m_mech, m_log, wasLimited);
    m_mech->start_limited_line(distance_type::fixup,
                               speed_type::fixup,
                               false);
}

void fixup::update() {
    // step 1: travelback
    if (!m_state.is_travelback_finished()) {
        if (!m_mech->motors_rotating()) {
            m_state.mark_travelback_finished();
            m_mech->start_limited_turn(m_state.target_direction(),
                                       speed_type::fixup);
        }
        // step 2: turn to the right direction
    } else {
        if (!m_mech->motors_rotating()) {
            m_state.mark_finished();
            m_state.restore_move(*m_mech, m_log);
        }
    }
}

bool fixup::running() const {
    return m_state.is_running();
}

fixup::cycle::cycle()
        : inProgress(false),
          travelbackOK(false),
          target(orient::north) {}

fixup::cycle::cycle(orient target,
                    mechanics &mech,
                    source log,
                    bool wasLimited)
        : inProgress(true),
          travelbackOK(false),
          target(target),
          capture(mech, wasLimited, log) {}

void fixup::cycle::mark_finished() {
    inProgress = false;
}

bool fixup::cycle::is_running() const {
    return inProgress;
}

bool fixup::cycle::is_travelback_finished() const {
    return travelbackOK;
}

void fixup::cycle::mark_travelback_finished() {
    travelbackOK = true;
}

maps::orient fixup::cycle::target_direction() {
    return target;
}

void fixup::cycle::restore_move(mechanics &mech, source log) {
    capture.restore(mech, log);
}
