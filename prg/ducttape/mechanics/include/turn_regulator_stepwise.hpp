#ifndef DUCTTAPE_TURN_REGULATOR_STEPWISE_HPP
#define DUCTTAPE_TURN_REGULATOR_STEPWISE_HPP

#include "turn_regulator_base.hpp"

namespace ducttape {
    class turn_regulator_stepwise : public turn_regulator_base {
    public:
        using orient = robofat2::maps::orient;

        turn_regulator_stepwise(robofat2::settings::settings &conf,
                                movement_model &model,
                                motor_pair &motors,
                                sensorics_base &sensors,
                                std::shared_ptr<robofat2::logging::sink> logDevice);

    protected:
        void do_start() override;
        void do_run() override;
        void do_stop() override;

    private:
        int get_fix_angle();
        void do_turn();
        void do_turn(int fix);

        int m_threshold;
        robofat2::logging::source m_log;
    };
}

#endif //DUCTTAPE_TURN_REGULATOR_STEPWISE_HPP
