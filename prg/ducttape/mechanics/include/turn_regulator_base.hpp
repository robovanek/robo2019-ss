#ifndef DUCTTAPE_TURN_REGULATOR_BASE_HPP
#define DUCTTAPE_TURN_REGULATOR_BASE_HPP

#include <movement_model.hpp>
#include <sensorics.hpp>
#include <robofat2/diffbot/motor_pair.hpp>
#include <robofat2/maps/heading.hpp>

namespace ducttape {
    class turn_regulator_base : public robofat2::threading::task {
    public:
        turn_regulator_base(movement_model &model,
                            motor_pair &motors,
                            sensorics_base &sensors);

        void start(robofat2::maps::orient target, angular_speed_pair<int> speed);

        bool is_running() const;

    protected:
        void update() override;

        virtual void do_start() = 0;
        virtual void do_run() = 0;
        virtual void do_stop() = 0;

        void mark_running();
        void mark_finished();

    protected:
        movement_model *m_model;
        motor_pair *m_motors;
        sensorics_base *m_sensors;
        robofat2::maps::orient m_targetDirection;
        robofat2::maps::heading m_targetHeading;
        angular_speed_pair<int> m_setSpeeds;

    private:
        bool m_running;
    };
}

#endif //DUCTTAPE_TURN_REGULATOR_BASE_HPP
