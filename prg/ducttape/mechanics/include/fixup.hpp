#ifndef DUCTTAPE_FIXUP_HPP
#define DUCTTAPE_FIXUP_HPP

#include <robofat2/settings/settings.hpp>
#include <robofat2/logging/sink.hpp>
#include <robofat2/logging/source.hpp>
#include <robofat2/maps/heading.hpp>
#include <robofat2/diffbot/motor_types.hpp>
#include <chrono>

namespace ducttape {
    using namespace robofat2::diffbot;

    class mechanics;

    class fixup_capture {
    public:
        fixup_capture();

        explicit fixup_capture(mechanics &mech, bool wasLimited, robofat2::logging::source log);

        void restore(mechanics &mech, robofat2::logging::source log);

    private:
        using clock = std::chrono::steady_clock;

        int getCompletedDegrees(angular_pos_pair<int> restoreTacho) const;

        int getRemainingDegrees(int completed) const;

        // old final values
        angular_pos_pair<int> m_targetTacho{};
        // start-time memorized values
        angular_pos_pair<int> m_startTacho{};
        clock::time_point m_startTime;
        // capture-time values
        angular_pos_pair<int> m_captureTacho{};
        clock::time_point m_captureTime;
        // configured speed
        angular_speed_pair<int> m_speed{};
        // move type
        bool m_wasLimited{};
    };

    class fixup {
    public:
        using orient = robofat2::maps::orient;

        explicit fixup(ducttape::mechanics &mech, std::shared_ptr<robofat2::logging::sink> log);

        void start(orient target, bool wasLimited);

        void update();

        bool running() const;

    private:
        struct cycle {
        public:
            cycle();

            explicit cycle(orient target, mechanics &mech, robofat2::logging::source log, bool wasLimited);

            orient target_direction();

            void mark_finished();

            void mark_travelback_finished();

            bool is_running() const;

            bool is_travelback_finished() const;

            void restore_move(mechanics &mech, robofat2::logging::source log);

        private:
            bool inProgress;
            bool travelbackOK;
            orient target;
            fixup_capture capture;
        };

        mechanics *m_mech;
        cycle m_state;
        robofat2::logging::source m_log;
    };
}

#endif //DUCTTAPE_FIXUP_HPP
