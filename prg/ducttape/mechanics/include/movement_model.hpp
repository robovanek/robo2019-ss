#ifndef DUCTTAPE_MOVEMENT_MODEL_HPP
#define DUCTTAPE_MOVEMENT_MODEL_HPP

#include <robofat2/diffbot/motor_types.hpp>
#include <robofat2/diffbot/movement_model.hpp>
#include <robofat2/settings/settings.hpp>
#include <unordered_map>

namespace ducttape {
    using namespace robofat2::diffbot;

    enum class speed_type {
        free_travel,
        travelback,
        climbup_lower,
        climbup_upper,
        climbdown,
        turn,
        fixup,
        kamikaze,
        fail
    };

    enum class distance_type {
        travelback_too_deep,
        travelback_too_high,
        travelback_single,
        initial_travel,
        after_climb_travel,
        after_fall_travel,
        climb_upper,
        climb_lower_pregyro,
        climb_lower_postgyro,
        fall_upper,
        fall_lower,
        block,
        fixup,
        kamikaze,
        fail
    };

    class movement_model : public robofat2::diffbot::movement_model {
    public:
        explicit movement_model(robofat2::settings::settings &conf);

        angular_pos_pair<int> calculate_tacho_for_travel(distance_type type) const;

        // type to value
        float get_linear_speed(speed_type type) const;

        float get_linear_distance(distance_type type) const;

        angular_speed_pair<int> get_angular_speed_pair(speed_type type) const;

    private:
        std::unordered_map<speed_type, float> m_speeds;
        std::unordered_map<distance_type, float> m_distances;
    };
}

#endif //DUCTTAPE_MOVEMENT_MODEL_HPP
