#ifndef DUCTTAPE_ARM_HPP
#define DUCTTAPE_ARM_HPP

#include <ev3dev.h>
#include <robofat2/settings/settings.hpp>
#include <unordered_map>
#include <robofat2/logging/source.hpp>

namespace ducttape {
    enum class arm_action {
        revert_to_normal,
        prepare_for_pushdown,
        pushdown,
        fall_balancer,
        fail
    };

    struct action_info {
        float fraction_top;
        int speed;
    };

    class arm_controller {
    public:
        explicit arm_controller(robofat2::settings::settings &conf,
                                std::shared_ptr<robofat2::logging::sink> log);

        ~arm_controller();

        void calibrate();

        void perform(arm_action action);

        void stop();

        bool moving();

    private:
        void initialize_motor(robofat2::settings::settings &conf);

        void perform(const action_info &info);

        void calibrate_step(int &finalTacho, int speed);

        using motor_setter = ev3dev::motor &(ev3dev::motor::*)(int);

        ev3dev::medium_motor m_armMotor;
        int m_topAngle;
        int m_bottomAngle;
        std::unordered_map<arm_action, action_info> m_actions;
        robofat2::logging::source m_log;
    };
}

#endif //DUCTTAPE_ARM_HPP
