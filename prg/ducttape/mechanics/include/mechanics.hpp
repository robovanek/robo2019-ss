#ifndef DUCTTAPE_MECHANICS_HPP
#define DUCTTAPE_MECHANICS_HPP


#include <robofat2/diffbot/motor_types.hpp>
#include <robofat2/diffbot/motor_pair.hpp>
#include "straight_regulator.hpp"
#include "turn_regulator_base.hpp"

namespace ducttape {
    using namespace robofat2::diffbot;
    class fixup_capture;

    class mechanics {
    private:
        friend class fixup_capture;
        using orient = robofat2::maps::orient;
    public:
        explicit mechanics(robofat2::settings::settings &conf,
                           sensorics_base &sensorics,
                           spatial::environment &space,
                           const std::shared_ptr<robofat2::logging::sink>& log);

        void start_unlimited_line(speed_type speed, bool regulate = true);
        void start_limited_line(distance_type distance, speed_type speed, bool regulate = true);
        void start_limited_line(distance_type distance, speed_type speed, int amount, bool regulate = true);
        void start_limited(angular_pos_pair<int> deltas, speed_type speed, bool regulate = true);
        void start_limited_turn(orient target, speed_type speed);
        void stop();

        float distance_travelled_since_start();
        float angle_turned_since_start();
        std::chrono::milliseconds time_elapsed_since_start();

        bool has_travelled_since_start(distance_type type);

        bool moving();
        bool motors_rotating();
        bool fixup_running();

        straight_regulator &straight_reg();

        turn_regulator_base &turn_reg();

    private:
        static turn_regulator_base *initializeTurnReg(robofat2::settings::settings &conf,
                                                      movement_model &model,
                                                      robofat2::diffbot::motor_pair &motors,
                                                      sensorics_base &sensors,
                                                      std::shared_ptr<robofat2::logging::sink> log);
        angular_pos_pair<int> get_wheel_deltas();
        void store_start_state();

        robofat2::diffbot::motor_pair m_motors;
        movement_model m_model;
        sensorics_base *m_sensors;
        spatial::environment *m_space;

        straight_regulator m_straight;
        std::unique_ptr<turn_regulator_base> m_turn;
        robofat2::utilities::stopwatch m_startTimer;
        angular_pos_pair<int> m_startTacho = {0, 0};
        angular_speed_pair<int> m_startSpeed = {0, 0};
    };
}

#endif //DUCTTAPE_MECHANICS_HPP
