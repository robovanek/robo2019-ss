#ifndef DUCTTAPE_TURN_REGULATOR_CONTINUOUS_HPP
#define DUCTTAPE_TURN_REGULATOR_CONTINUOUS_HPP

#include <robofat2/pid/pid.hpp>
#include "turn_regulator_base.hpp"

namespace ducttape {
    class turn_regulator_continuous : public turn_regulator_base {
    public:
        turn_regulator_continuous(robofat2::settings::settings &conf,
                                  movement_model &model,
                                  motor_pair &motors,
                                  sensorics_base &sensors);

    protected:
        void do_start() override;

        void do_run() override;

        void do_stop() override;

    private:
        bool isFinished(float rawControl) const;

        robofat2::pid::pid m_pid;
        int m_speedLimit;
        float m_proportionalThreshold;
        float m_integralThreshold;
        float m_derivativeThreshold;
    };
}

#endif //DUCTTAPE_TURN_REGULATOR_CONTINUOUS_HPP
