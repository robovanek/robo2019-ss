#ifndef DUCTTAPE_STRAIGHT_REGULATOR_HPP
#define DUCTTAPE_STRAIGHT_REGULATOR_HPP

#include <robofat2/threading/task.hpp>
#include <robofat2/settings/settings.hpp>
#include <robofat2/diffbot/motor_pair.hpp>
#include <robofat2/logging/sink.hpp>
#include <robofat2/logging/source.hpp>
#include <robofat2/utilities/stopwatch.hpp>
#include <robofat2/pid/pid.hpp>
#include <sensorics.hpp>
#include <environment.hpp>
#include "fixup.hpp"

namespace ducttape {
    class mechanics;

    class straight_regulator : public robofat2::threading::task {
    public:
        straight_regulator(robofat2::settings::settings &conf,
                           motor_pair &motors,
                           sensorics_base &sensors,
                           spatial::environment &space,
                           mechanics &mech,
                           const std::shared_ptr<robofat2::logging::sink> &logDevice);

        void enable(angular_speed_pair<int> baseSpeeds, bool limited);

        void disable();

        bool fuckup_fixup_running() const;

    protected:
        void update() final;

    private:
        void gyro_update();

        int get_gyro_fix();

        float get_correction(int gyroFix);

        void push(float correction);

        mechanics *m_mech;
        motor_pair *m_motors;
        sensorics_base *m_sensors;
        spatial::environment *m_space;
        fixup m_fixup;
        int m_fuckupThreshold;
        robofat2::pid::pid m_gyroStraightPid;
        float m_gyroMaxCorrection;
        float m_gyroIntegralFreeze;
        bool m_canBeEnabled;

        bool m_started = false;
        angular_speed_pair<int> m_baseSpeed = {0, 0};
        bool m_active = false;
        bool m_limited = false;
        robofat2::logging::source m_log;
    };
}

#endif //DUCTTAPE_STRAIGHT_REGULATOR_HPP
