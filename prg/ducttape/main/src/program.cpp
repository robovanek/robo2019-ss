#include <program.hpp>
#include <router_straight.hpp>
#include <router_sweep.hpp>
#include <router_dijkstra.hpp>
#include <robofat2/logging/stderr_sink.hpp>

using namespace ducttape;
using namespace robofat2::logging;
using namespace robofat2::settings;
using namespace robofat2::maps;
using namespace robofat2::utilities;
using namespace robofat2::threading;

program::program(const std::string &confpath, const std::string &algo)
        : m_conf(confpath),
          m_log(stderr_sink::make()),
          m_scheduler(source("scheduler", m_log)),
          m_idleTask(m_conf.get_integer("scheduler/loop_time")),
          m_environment(m_conf, m_log),
          m_spatialReasoning(initializeSpatial(algo, m_environment, m_conf, m_log)),
          m_sensors(new sensorics(m_conf, m_log)),
          m_mechanics(m_conf, *m_sensors, m_environment, m_log),
          m_arm(m_conf, m_log),
          m_executive(m_mechanics, *m_sensors, m_arm, *m_spatialReasoning, m_environment, m_conf, m_log) {
    m_scheduler.append(*m_sensors);
    m_scheduler.append(m_executive);
    m_scheduler.append(m_mechanics.straight_reg());
    m_scheduler.append(m_mechanics.turn_reg());
    m_scheduler.append(m_idleTask);
}

void program::calibrate_gyro() {
    m_sensors->gyro_calibrate();
}

void program::calibrate_arm() {
    m_arm.calibrate();
}

void program::enter() {
    m_scheduler.enterLoop();
}

spatial::router_base *program::initializeSpatial(const std::string &mode,
                                                 spatial::environment &env,
                                                 robofat2::settings::settings &conf,
                                                 std::shared_ptr<robofat2::logging::sink> log) {
    if (mode == "straight")
        return new spatial::router_straight(env, conf, log);
    else if (mode == "sweep")
        return new spatial::router_sweep(env, conf, log);
    else if (mode == "dijkstra")
        return new spatial::router_dijkstra(env, conf, log);
    else
        throw std::runtime_error("invalid spatial reasoning mode selected: " + mode);
}

void program::set_initial_x(int x) {
    m_environment.set_initial_x(x);
    m_spatialReasoning->reset();
}

void program::set_kamikaze(bool rly) {
    m_executive.consts().set_kamikaze(rly);
}
