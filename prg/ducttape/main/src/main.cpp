#include <string>
#include <iostream>
#include <memory>
#include <thread>
#include <robofat2/backtrace.hpp>

#include <main.hpp>
#include <program.hpp>
#include <robofat2/display/display.hpp>

const char *default_config_path = "/home/robot/etc/ducttape.properties";

int main(int argc, char **argv) {
    robofat2::backtrace::initialize();
    // font size
    system("setfont Lat2-Terminus12x6");

    auto pPrg = init_phase(argc, argv);
    arm_phase(*pPrg);
    prompt_kamikaze(*pPrg);
    prompt_phase(*pPrg);
    wait_phase(*pPrg);
    run_phase(*pPrg);
}

std::unique_ptr<ducttape::program> init_phase(int argc, char **argv) {
    std::string confpath = default_config_path;
    if (argc >= 2) {
        confpath = argv[1];
    }

    std::cerr << "Config path: " << confpath << std::endl << std::flush;
    std::cerr << "Initializing program..." << std::endl << std::flush;
    return std::unique_ptr<ducttape::program>(
            new ducttape::program(confpath,
                    prompt_algo())
    );
}

void arm_phase(ducttape::program &prg) {
    std::cerr << "Calibrating arm..." << std::endl << std::flush;
    prg.calibrate_arm();
}

void prompt_kamikaze(ducttape::program &prg) {
    long select = robofat2::display::show_menu("Kamikaze:", {"OFF", "ON", "CANCEL"});
    if (select == -1 || select == 2) {
        std::exit(0);
    }
    if (select == 0) {
        prg.set_kamikaze(false);
    } else if (select == 1) {
        prg.set_kamikaze(true);
    }
}

std::string prompt_algo() {
    long select = robofat2::display::show_menu("Algo:", {"DIJKSTRA", "SWEEP", "STRAIGHT", "CANCEL"});
    if (select == -1 || select == 3) {
        std::exit(0);
    }
    if (select == 0) {
        return "dijkstra";
    } else if (select == 1) {
        return "sweep";
    } else if (select == 2) {
        return "straight";
    } else {
        return "dijkstra";
    }
}

void prompt_phase(ducttape::program &prg) {
    long select = robofat2::display::show_menu("X + gyro:", {"1", "2", "3", "4", "5", "6", "CANCEL"});
    if (select == -1 || select == 6) {
        std::exit(0);
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(500));
    prg.set_initial_x((int) select);
    std::cerr << "Calibrating gyro..." << std::endl << std::flush;
    prg.calibrate_gyro();
}

void wait_phase(ducttape::program &prg) {
    long select = robofat2::display::show_menu("Start round:", {"PROCEED", "CANCEL"});
    if (select == -1 || select == 1) {
        std::exit(0);
    }
}

void run_phase(ducttape::program &prg) {
    std::cerr << "Go!" << std::endl << std::flush;
    prg.enter();
    std::cerr << "Exit! " << std::endl << std::flush;
}
