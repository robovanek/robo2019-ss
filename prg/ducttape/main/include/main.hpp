#ifndef DUCTTAPE_MAIN_HPP
#define DUCTTAPE_MAIN_HPP

#include "program.hpp"

extern const char *default_config_path;

extern int main(int argc, char **argv);
extern std::unique_ptr<ducttape::program> init_phase(int argc, char **argv);
extern void arm_phase(ducttape::program &prg);
extern void prompt_phase(ducttape::program &prg);
extern void prompt_kamikaze(ducttape::program &prg);
extern std::string prompt_algo();
extern void wait_phase(ducttape::program &prg);
extern void run_phase(ducttape::program &prg);

#endif //DUCTTAPE_MAIN_HPP
