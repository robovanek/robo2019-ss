#ifndef DUCTTAPE_PROGRAM_HPP
#define DUCTTAPE_PROGRAM_HPP

#include <string>
#include <memory>
#include <router_base.hpp>
#include <sensorics.hpp>
#include <arm.hpp>
#include <mechanics.hpp>
#include <executive.hpp>
#include <robofat2/threading/idle_task.hpp>
#include <robofat2/threading/scheduler.hpp>

namespace ducttape {
    class program {
    public:
        explicit program(const std::string &confpath, const std::string &algo);

        void calibrate_gyro();

        void calibrate_arm();

        void set_initial_x(int x);

        void set_kamikaze(bool rly);

        void enter();

    private:
        static spatial::router_base *initializeSpatial(const std::string &mode,
                                                       spatial::environment &env,
                                                       robofat2::settings::settings &conf,
                                                       std::shared_ptr<robofat2::logging::sink> log);
        robofat2::settings::settings m_conf;
        std::shared_ptr<robofat2::logging::sink> m_log;
        robofat2::threading::scheduler m_scheduler;
        robofat2::threading::idle_task m_idleTask;
        spatial::environment m_environment;
        std::unique_ptr<spatial::router_base> m_spatialReasoning;
        std::unique_ptr<sensorics_base> m_sensors;
        mechanics m_mechanics;
        arm_controller m_arm;
        executive m_executive;
    };
}

#endif //DUCTTAPE_PROGRAM_HPP
