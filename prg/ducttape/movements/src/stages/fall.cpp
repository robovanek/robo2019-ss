#include <stages/forward.hpp>
#include <stages/hybrid_wait.hpp>
#include <stages/gyro_reset.hpp>
#include "stages/fall.hpp"
#include "mechanics.hpp"
#include "arm.hpp"
#include "environment.hpp"

using namespace ducttape;
using namespace ducttape::stages;

static void startFall(ducttape::executive &exec) {
    exec.move().start_unlimited_line(ducttape::speed_type::climbdown,
                                     !exec.consts().is_align_enabled(align_type::after_fall));
}

static void activateArm(ducttape::executive &exec) {
    exec.arm().perform(arm_action::fall_balancer);
}

static void retractArm(ducttape::executive &exec) {
    exec.arm().perform(arm_action::revert_to_normal);
}

static void reportComplete(executive &exec) {
    exec.env().record_climb_down();
}

fall::fall(executive &exec, std::string name)
        : stage_queue(exec, std::move(name)) {
    this
            ->next<command>(startFall, "start climbing down the wall")

            ->next<command>(activateArm, "push down arm for better balancing")
            ->next<hybrid_wait>(wait_type::fall_upper, "wait until robot is stabilized")

            ->next<command>(retractArm, "revert arm to normal position")
            ->next<gyro_reset>(align_type::after_fall, "reset gyro angle to a known value")
            ->next<hybrid_wait>(wait_type::fall_lower, "wait until arm is up")

            ->next<command>(reportComplete, "report that fall was completed")
            ->follow_with<forward>(distance_type::after_fall_travel, 1, false, "go to the block center after fall");
}
