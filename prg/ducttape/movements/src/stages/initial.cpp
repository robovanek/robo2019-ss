#include <stages/initial.hpp>
#include <stages/turn.hpp>

using namespace ducttape;
using namespace ducttape::stages;

initial::initial(executive &exec)
        : stage(exec, "Initial stage") {}

loop initial::operator()() {
    return follow_with<decision_point>("initial travel to next block");
}
