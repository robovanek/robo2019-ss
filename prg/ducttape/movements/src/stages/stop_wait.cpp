#include "stages/stop_wait.hpp"
#include "mechanics.hpp"

using namespace ducttape;
using namespace ducttape::stages;

stop_wait::stop_wait(executive &exec, std::string name)
        : stage(exec, std::move(name)) {}

loop stop_wait::operator()() {
    return ctx.move().moving() ? loop::again : loop::exit;
}
