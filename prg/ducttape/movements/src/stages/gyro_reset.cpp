#include "stages/gyro_reset.hpp"
#include "sensorics.hpp"
#include "environment.hpp"

using namespace robofat2;
using namespace ducttape;
using namespace ducttape::spatial;

stages::gyro_reset::gyro_reset(executive &exec, align_type type, std::string &&name)
        : stage(exec, std::move(name)),
          m_type(type) {}

loop stages::gyro_reset::operator()() {
    run(ctx, m_type);
    return loop::exit;
}

void stages::gyro_reset::run(executive &exec, align_type type) {
    if (exec.consts().is_align_enabled(type)) {
        exec.sense().gyro_align(exec.env().get_current_direction());
        exec.env().record_gyro_alignment();
    }
}
