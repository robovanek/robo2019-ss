#include "stages/arm_wait.hpp"
#include "arm.hpp"

using namespace ducttape;
using namespace ducttape::stages;

arm_wait::arm_wait(executive &exec, std::string name)
        : stage(exec, std::move(name)) {}

loop arm_wait::operator()() {
    if (ctx.arm().moving())
        return loop::again;
    else
        return loop::exit;
}
