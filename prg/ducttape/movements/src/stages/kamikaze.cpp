#include <stages/kamikaze.hpp>
#include <stages/stop_wait.hpp>
#include <stages/forward.hpp>
#include <stages/turn.hpp>
#include <environment.hpp>
#include <mechanics.hpp>

using namespace ducttape;
using namespace ducttape::stages;

static void startDeath(ducttape::executive &exec) {
    exec.move().start_limited_line(ducttape::distance_type::kamikaze,
                                   ducttape::speed_type::kamikaze,
                                   true);
}

static void reportComplete(executive &exec, int depth) {
    exec.env().record_kamikaze(depth);
}

kamikaze::kamikaze(executive &exec, int depth, std::string name)
        : stage_queue(exec, std::move(name)) {
    this
            ->next<command>(startDeath, "start jumping from the cliff")
            ->next<stop_wait>("wait until we are far enough")
            ->next<command>(std::bind(reportComplete, std::placeholders::_1, depth), "report that jump was completed")
            ->follow_with<decision_point>("decide what to do now");
}
