#include <stages/climb.hpp>
#include <stages/turn.hpp>
#include <stages/gyro_reset.hpp>
#include <stages/kamikaze.hpp>
#include <stages/fail_recovery.hpp>
#include "stages/travelback.hpp"
#include "stages/fall.hpp"
#include "stages/forward.hpp"
#include "mechanics.hpp"
#include "environment.hpp"

using namespace ducttape;
using namespace ducttape::stages;

forward::forward(executive &exec,
                 distance_type checkpoint,
                 int amount,
                 bool just_align,
                 std::string &&name)
        : stage(exec, std::move(name)),
          m_first(true),
          m_checkpointDistance(checkpoint),
          m_checkpointAmount(amount),
          m_justAlign(just_align),
          m_log(exec.log("fwd")) {}

loop forward::operator()() {
    if (m_first) {
        m_first = false;
        return doStart();
    }
    // do not attempt to do anything (i.e. screw it)
    if (ctx.move().fixup_running()) {
        return loop::again;
    }

    if (singleWall()) {
        if (m_justAlign) {
            gyro_reset::run(ctx, align_type::after_align);
            return follow_with<travelback>(wait_type::after_color_contact,
                                           distance_type::travelback_single, 1,
                                           "align to a single wall successful, return to center");
        } else {
            // oops, climbing immediately again seems wrong
            // consider the climb failed
            // however this might backfire if there are multiple single steps in one line
            if (m_checkpointDistance == distance_type::after_climb_travel) {
                ctx.env().record_last_climb_up_failed();
            } else {
                notifyOfStop();
            }
            return follow_with<climb>("wall was detected, climb up the wall");
        }
    }
    if (highWall()) {
        notifyOfStop();
        if (m_justAlign) {
            gyro_reset::run(ctx, align_type::after_align);
            return follow_with<travelback>(wait_type::after_touch_contact,
                                           distance_type::travelback_too_high, 2,
                                           "align to a single wall successful, return to center");
        } else {
            gyro_reset::run(ctx, align_type::after_high_wall_hit);
            return follow_with<travelback>(wait_type::after_touch_contact,
                                           distance_type::travelback_too_high, 2,
                                           "too high wall, back off");
        }
    }
    if (m_checkpointDistance != distance_type::after_climb_travel &&
        m_checkpointDistance != distance_type::after_fall_travel) {
        auto depth = sonic();
        if (ctx.consts().do_kamikaze()) {
            switch (depth) {
                case sonic_result::indeterminate:
                case sonic_result::no_cliff:
                    break;
                case sonic_result::cliff_down_1:
                case sonic_result::cliff_down_2:
                case sonic_result::cliff_down_3:
                case sonic_result::cliff_down_4:
                case sonic_result::cliff_down_5:
                case sonic_result::cliff_down_6:
                case sonic_result::cliff_down_disaster:
                    notifyOfStop();
                    return follow_with<kamikaze>(-(int) depth,
                                                 "let's be brave and jump full-speed from cliff");
            }
        } else {
            switch (depth) {
                case sonic_result::indeterminate:
                case sonic_result::no_cliff:
                    break;
                case sonic_result::cliff_down_1:
                    notifyOfStop();
                    return follow_with<fall>("small cliff, climb down the wall");
                case sonic_result::cliff_down_2:
                case sonic_result::cliff_down_3:
                case sonic_result::cliff_down_4:
                case sonic_result::cliff_down_5:
                case sonic_result::cliff_down_6:
                case sonic_result::cliff_down_disaster:
                    notifyOfStop();
                    return follow_with<travelback>(wait_type::none,
                                                   distance_type::travelback_too_deep, -(int) depth,
                                                   "too deep cliff, back off");
            }
        }
    } else if (m_checkpointDistance == distance_type::after_climb_travel) {
        int sonic = ctx.sense().rawSonic();
        if (sonic >= ctx.consts().failThreshold() &&
            sonic != 2550) {
            ctx.env().record_last_climb_up_failed();
            return follow_with<fail_recovery>("recover fail B");
        }
    }
    if (hasReachedCheckpoint()) {
        notifyOfStop();
        return follow_with<decision_point>("checkpoint reached, decide and turn");
    }
    return loop::again;
}

bool forward::singleWall() {
    return ctx.sense().single_wall_detected();
}

bool forward::highWall() {
    return ctx.sense().high_wall_detected();
}

void forward::notifyOfStop() {
    ctx.env().record_forward(ctx.move().distance_travelled_since_start());
}

loop forward::doStart() {
    ctx.move().start_limited_line(m_checkpointDistance, speed_type::free_travel, m_checkpointAmount, !m_justAlign);
    return loop::again;
}

sonic_result forward::sonic() {
    return ctx.sense().analyze_sonic();
}

bool forward::hasReachedCheckpoint() {
    return !ctx.move().moving();
}
