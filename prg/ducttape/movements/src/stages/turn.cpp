#include <stages/forward.hpp>
#include <stages/stop_wait.hpp>
#include <stages/hybrid_wait.hpp>
#include "stages/turn.hpp"
#include "mechanics.hpp"
#include "router_base.hpp"
#include "environment.hpp"
#include <functional>

using namespace ducttape;
using namespace ducttape::stages;
using namespace ducttape::spatial;
using namespace robofat2::maps;
using std::placeholders::_1;

decision_point::decision_point(executive &exec, std::string name)
        : stage(exec, std::move(name)) {}

loop decision_point::operator()() {
    auto origin = ctx.env().get_current_direction();
    decision order = ctx.decisions().get_next_direction();

    if (origin == order.direction_where) {
        return follow_with<forward>(distance_type::block,
                                    order.blocks_forward,
                                    order.just_align,
                                    "no turn performed; go forward to next block");
    } else {
        return follow_with<turn_only>(order,
                                      "let's turn to the necessary direction: " + orient_as_string(order.direction_where));
    }
}

static void stopMotors(executive &exec) {
    exec.move().stop();
}

static void startTurn(orient target, executive &exec) {
    exec.move().start_limited_turn(target, speed_type::turn);
    exec.env().record_turned(target);
}

turn_only::turn_only(executive &exec, decision order, std::string name)
        : stage_queue(exec, std::move(name)) {
    this
            ->next<command>(stopMotors, "stop motors before proceeding")
            ->next<hybrid_wait>(wait_type::pre_turn, "let the robot rest for a while before turning")
            ->next<command>(std::bind(startTurn, order.direction_where, _1), "start turning")
            ->next<stop_wait>("wait until the turn finishes")
            ->next<hybrid_wait>(wait_type::post_turn, "let the robot rest for a while after turning")
            ->follow_with<forward>(distance_type::block, order.blocks_forward, order.just_align,
                                   "go to the center of the next block");
}
