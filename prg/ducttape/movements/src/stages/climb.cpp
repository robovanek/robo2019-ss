#include <stages/forward.hpp>
#include <stages/hybrid_wait.hpp>
#include <stages/arm_wait.hpp>
#include <stages/gyro_reset.hpp>
#include "stages/climb.hpp"
#include "environment.hpp"
#include "mechanics.hpp"
#include "arm.hpp"

using namespace ducttape;
using namespace ducttape::stages;
using std::placeholders::_1;

static void start_lower_climb(ducttape::executive &exec);
static void start_upper_climb(ducttape::executive &exec);
static void finish_climb(ducttape::executive &exec);
static void report_to_brain(ducttape::executive &exec);

climb::climb(executive &exec,
             std::string name)
        : stage_queue(exec, std::move(name)) {
    this
            ->next<command>(start_lower_climb, "start climbing the wall")
            ->next<hybrid_wait>(wait_type::climb_lower_pregyro, "wait until lower climb is complete (pregyro)")
            ->next<gyro_reset>(align_type::during_climb, "reset gyro angle to a known value")
            ->next<hybrid_wait>(wait_type::climb_lower_postgyro, "wait until lower climb is complete (postgyro)")

            ->next<command>(start_upper_climb, "continue climbing the wall")
            //->next<hybrid_wait>(wait_type::climb_upper, "wait until upper climb is complete")
            ->next<hybrid_wait>(wait_type::climb_upper, "wait until upper climb is complete")

            ->next<command>(finish_climb, "finish wall climbing")
            ->next<command>(report_to_brain, "report that wall climbing was completed")

            ->follow_with<forward>(distance_type::after_climb_travel, 1, false, "go to the block center after climb");
}

void start_lower_climb(ducttape::executive &exec) {
    exec.move().start_unlimited_line(speed_type::climbup_lower,
                                     !exec.consts().is_align_enabled(align_type::during_climb));
    exec.arm().perform(arm_action::prepare_for_pushdown);
}

void start_upper_climb(ducttape::executive &exec) {
    exec.move().start_unlimited_line(speed_type::climbup_upper,
                                     true);
    exec.arm().perform(arm_action::pushdown);
}

void finish_climb(ducttape::executive &exec) {
    exec.arm().perform(arm_action::revert_to_normal);
}

void report_to_brain(ducttape::executive &exec) {
    exec.env().record_climb_up();
}
