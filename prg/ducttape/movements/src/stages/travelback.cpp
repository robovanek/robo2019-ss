#include <functional>
#include <stages/stop_wait.hpp>
#include <stages/arm_wait.hpp>
#include "stages/turn.hpp"
#include "stages/travelback.hpp"
#include "environment.hpp"
#include "mechanics.hpp"
#include "arm.hpp"

using namespace ducttape;
using namespace ducttape::stages;
using std::placeholders::_1;

typedef void (*function_t)(executive &exec);

static void startMove(distance_type type, executive &exec) {
    exec.move().start_limited_line(type, speed_type::travelback, false);
}

static void report(int delta, executive &exec) {
    if (delta > 0) {
        exec.env().record_travelback_high(delta);
    } else {
        exec.env().record_travelback_deep(-delta);
    }
}

static void armDown(executive &exec) {
    exec.arm().perform(arm_action::fail);
}

static void armUp(executive &exec) {
    exec.arm().perform(arm_action::revert_to_normal);
}

travelback::travelback(executive &exec, wait_type before, distance_type type, int delta, std::string name)
        : stage_queue(exec, std::move(name)) {
    this
            ->next<hybrid_wait>(before, "wait before travelling back")
            ->next<command>(std::bind(startMove, type, _1), "start moving backwards")
            ->next<stop_wait>("wait until the backwards move stops")
            ->next<command>(std::bind(report, delta, _1), "report that travelback has been finished")

            ->next<command>(armDown, "arm recovery")
            ->next<arm_wait>("arm wait")
            ->next<command>(armUp, "arm recovery revert")
            ->next<arm_wait>("arm wait")

            ->follow_with<decision_point>("decide and turn");
}
