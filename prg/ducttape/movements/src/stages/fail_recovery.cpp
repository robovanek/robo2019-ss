//
// Created by kuba on 22.11.19.
//

#include <stages/stop_wait.hpp>
#include <stages/arm_wait.hpp>
#include <stages/forward.hpp>
#include "stages/fail_recovery.hpp"
#include <arm.hpp>
#include <mechanics.hpp>
#include <stages/hybrid_wait.hpp>

using namespace ducttape;
using namespace ducttape::stages;


static void fail_goBack(executive &exec) {
    exec.move().start_limited_line(distance_type::fail,
                                   speed_type::fail,
                                   1,
                                   false);
}

static void startArm(executive &exec) {
    exec.arm().perform(arm_action::fail);
}

static void retractArm(executive &exec) {
    exec.arm().perform(arm_action::revert_to_normal);
}

ducttape::stages::fail_recovery::fail_recovery(ducttape::executive &exec, std::string name)
        : stage_queue(exec, std::move(name)) {
    this
            ->next<command>(fail_goBack, "go back a little")
            ->next<stop_wait>("wait until go back is complete")
            ->next<command>(startArm, "start arm recovery")
            ->next<arm_wait>("wait until recovery finishes")
            ->next<hybrid_wait>(wait_type::fail, "wait after fail")
            ->next<command>(retractArm, "retract arm")
            ->next<arm_wait>("wait until arm finishes")
            ->follow_with<forward>(distance_type::block, 1, false, "go forward");

}
