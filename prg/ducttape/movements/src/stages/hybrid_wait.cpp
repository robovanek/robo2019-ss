#include "stages/hybrid_wait.hpp"
#include "mechanics.hpp"

using namespace ducttape;
using namespace ducttape::stages;

struct wait_info {
    wait_info(std::string name,
              duration_type time,
              distance_type distance)
            : name(std::move(name)),
              time(time),
              distance(distance) {}

    std::string name;
    duration_type time;
    distance_type distance;
};

std::unordered_map<wait_type, wait_info> wait_infos = {
        {wait_type::climb_upper,          {"wait in upper climb part",
                                                  duration_type::climb_upper,
                                                  distance_type::climb_upper}},
        {wait_type::climb_lower_pregyro,  {"wait in lower climb part (before gyro reset)",
                                                  duration_type::climb_lower_pregyro,
                                                  distance_type::climb_lower_pregyro}},
        {wait_type::climb_lower_postgyro, {"wait in lower climb part (after gyro reset)",
                                                  duration_type::climb_lower_postgyro,
                                                  distance_type::climb_lower_postgyro}},
        {wait_type::fall_upper,           {"wait in upper fall part",
                                                  duration_type::fall_upper,
                                                  distance_type::fall_upper}},
        {wait_type::fall_lower,           {"wait in lower fall part",
                                                  duration_type::fall_lower,
                                                  distance_type::fall_lower}},
        {wait_type::pre_turn,             {"wait before turning",
                                                  duration_type::pre_turn,
                                                  distance_type::block}},
        {wait_type::post_turn,            {"wait after turning",
                                                  duration_type::post_turn,
                                                  distance_type::block}},
        {wait_type::after_touch_contact,  {"wait after touch contact",
                                                  duration_type::after_touch_contact,
                                                  distance_type::block}},
        {wait_type::after_color_contact,  {"wait after color contact",
                                                  duration_type::after_color_contact,
                                                  distance_type::block}},
        {wait_type::fail,                 {"fail",
                                                  duration_type::fail,
                                                  distance_type::block}},
        {wait_type::none,                 {"no wait",
                                                  duration_type::none,
                                                  distance_type::block}},
};


hybrid_wait::hybrid_wait(executive &exec,
                         wait_type wait,
                         std::string name)
        : stage(exec, std::move(name)) {
    const wait_info &info = wait_infos.at(wait);
    m_duration = exec.consts().duration_value(info.time);
    m_distance = info.distance;
}

loop hybrid_wait::operator()() {
    if (m_timer.is_mark_empty())
        m_timer.set_mark_to_now();

    if (distanceFinished()) {
        return loop::exit;
    }
    if (m_timer.has_elapsed_since_mark(m_duration)) {
        return loop::exit;
    }
    return loop::again;
}

bool hybrid_wait::distanceFinished() {
    return ctx.move().has_travelled_since_start(m_distance);
}

