#include <unordered_map>
#include "executive.hpp"
#include "stage.hpp"

#include <stages/initial.hpp>
#include <stages/turn.hpp>

using std::chrono::milliseconds;
using std::chrono::nanoseconds;
using ducttape::duration_type;
using ducttape::align_type;

std::unordered_map<duration_type, std::string> duration_conf_names = {
        {duration_type::climb_upper,          "planner/time/climb/up/upper"},
        {duration_type::climb_lower_pregyro,  "planner/time/climb/up/lower/pregyro"},
        {duration_type::climb_lower_postgyro, "planner/time/climb/up/lower/postgyro"},
        {duration_type::fall_upper,           "planner/time/climb/down/upper"},
        {duration_type::fall_lower,           "planner/time/climb/down/lower"},
        {duration_type::pre_turn,             "planner/time/turn/pre"},
        {duration_type::post_turn,            "planner/time/turn/post"},
        {duration_type::after_touch_contact,  "planner/time/contact/touch"},
        {duration_type::after_color_contact,  "planner/time/contact/color"},
        {duration_type::fail,                 "planner/time/fail"},
};

std::unordered_map<align_type, std::string> align_conf_names = {
        {align_type::after_high_wall_hit, "sensor/gyro/reset_on_button"},
        {align_type::during_climb,        "sensor/gyro/reset_on_climb"},
        {align_type::after_fall,          "sensor/gyro/reset_on_fall"},
        {align_type::after_align,         "sensor/gyro/reset_on_align"},
};

ducttape::consts_t::consts_t(robofat2::settings::settings &conf)
        : m_kamikaze(conf.get_bool("spatial/kamikaze")),
          m_failThreshold(conf.get_integer("spatial/fail_threshold")) {
    for (auto &&item : duration_conf_names) {
        m_times.emplace(item.first, conf.get_duration(item.second));
    }
    for (auto &&item : align_conf_names) {
        m_aligns.emplace(item.first, conf.get_bool(item.second));
    }
}


std::chrono::nanoseconds ducttape::consts_t::duration_value(ducttape::duration_type type) const {
    if (type == duration_type::none) {
        return nanoseconds(0);
    } else {
        return m_times.at(type);
    }
}

bool ducttape::consts_t::is_align_enabled(ducttape::align_type where) const {
    return m_aligns.at(where);
}

bool ducttape::consts_t::do_kamikaze() const {
    return m_kamikaze;
}

int ducttape::consts_t::failThreshold() const {
    return m_failThreshold;
}

void ducttape::consts_t::set_kamikaze(bool rly) {
    m_kamikaze = rly;
}

ducttape::executive::executive(mechanics &mechanics,
                               sensorics_base &sensorics,
                               arm_controller &arm,
                               spatial::router_base &decisions,
                               spatial::environment &environment,
                               robofat2::settings::settings &conf,
                               std::shared_ptr<robofat2::logging::sink> logDevice)
        : task("upper executive"),
          stage_queue(*this, "executive"),
          m_mechanics(&mechanics),
          m_sensorics(&sensorics),
          m_arm(&arm),
          m_decisions(&decisions),
          m_environment(&environment),
          m_consts(conf),
          m_logDevice(std::move(logDevice)) {
    next<stages::initial>();
}

ducttape::mechanics &ducttape::executive::move() {
    return *m_mechanics;
}

ducttape::sensorics_base &ducttape::executive::sense() {
    return *m_sensorics;
}

ducttape::arm_controller &ducttape::executive::arm() {
    return *m_arm;
}

ducttape::spatial::router_base &ducttape::executive::decisions() {
    return *m_decisions;
}

ducttape::spatial::environment &ducttape::executive::env() {
    return *m_environment;
}

ducttape::consts_t &ducttape::executive::consts() {
    return m_consts;
}

void ducttape::executive::update() {
    loop action = (*this)();
    if (action == loop::exit) {
        next<stages::decision_point>("nothing to do - scheduling extra turn");
    }
}

robofat2::logging::source ducttape::executive::log(const std::string &component) {
    return robofat2::logging::source(component, m_logDevice);
}

void ducttape::executive::announce_stage(robofat2::sequencer::stage<ducttape::executive> &stage_) {
    log("stages").info(stage_.name());
}
