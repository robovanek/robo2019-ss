#ifndef DUCTTAPE_EXECUTIVE_HPP
#define DUCTTAPE_EXECUTIVE_HPP

#include <chrono>
#include <unordered_map>
#include <robofat2/settings/settings.hpp>
#include <robofat2/threading/task.hpp>
#include <robofat2/logging/sink.hpp>
#include <robofat2/logging/source.hpp>
#include "stage.hpp"

namespace ducttape {
    enum class duration_type {
        climb_upper,
        climb_lower_pregyro,
        climb_lower_postgyro,
        fall_upper,
        fall_lower,
        pre_turn,
        post_turn,
        after_touch_contact,
        after_color_contact,
        fail,
        none
    };

    enum class align_type {
        after_high_wall_hit,
        during_climb,
        after_fall,
        after_align
    };

    class consts_t {
    public:
        explicit consts_t(robofat2::settings::settings &conf);

        std::chrono::nanoseconds duration_value(duration_type type) const;

        bool is_align_enabled(align_type where) const;

        bool do_kamikaze() const;

        void set_kamikaze(bool rly);

        int failThreshold() const;

    private:
        std::unordered_map<duration_type, std::chrono::nanoseconds> m_times;
        std::unordered_map<align_type, bool> m_aligns;
        bool m_kamikaze;
        int m_failThreshold;
    };


    class mechanics;

    class sensorics_base;

    class arm_controller;
    namespace spatial {
        class router_base;

        class environment;
    }

    class executive : public robofat2::threading::task,
                      public stage_queue {
    public:
        explicit executive(mechanics &mechanics,
                           sensorics_base &sensorics,
                           arm_controller &arm,
                           spatial::router_base &decisions,
                           spatial::environment &environment,
                           robofat2::settings::settings &conf,
                           std::shared_ptr<robofat2::logging::sink> logDevice);

        mechanics &move();

        sensorics_base &sense();

        arm_controller &arm();

        spatial::router_base &decisions();

        spatial::environment &env();

        consts_t &consts();

        robofat2::logging::source log(const std::string &component);

        void announce_stage(stage &stage_);

    protected:
        void update() override;

    private:
        mechanics *m_mechanics;
        sensorics_base *m_sensorics;
        arm_controller *m_arm;
        spatial::router_base *m_decisions;
        spatial::environment *m_environment;
        consts_t m_consts;
        std::shared_ptr<robofat2::logging::sink> m_logDevice;
    };
}

#endif //DUCTTAPE_EXECUTIVE_HPP
