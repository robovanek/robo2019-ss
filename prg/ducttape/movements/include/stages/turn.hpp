#ifndef DUCTTAPE_TURN_HPP
#define DUCTTAPE_TURN_HPP

#include <router_base.hpp>
#include "executive.hpp"

namespace ducttape {
    namespace stages {
        class decision_point : public stage {
        public:
            decision_point(executive &exec, std::string name);

            loop operator()() override;
        };

        class turn_only : public stage_queue {
        public:
            turn_only(executive &exec,
                      ducttape::spatial::decision order,
                      std::string name);
        };
    }
}

#endif //DUCTTAPE_TURN_HPP
