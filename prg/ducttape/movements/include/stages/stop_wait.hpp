#ifndef DUCTTAPE_STOP_WAIT_HPP
#define DUCTTAPE_STOP_WAIT_HPP

#include "executive.hpp"

namespace ducttape {
    namespace stages {
        class stop_wait : public stage {
        public:
            stop_wait(executive &exec, std::string name);

            loop operator()() override;
        };
    }
}

#endif //DUCTTAPE_STOP_WAIT_HPP
