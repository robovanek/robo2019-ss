#ifndef DUCTTAPE_FALL_HPP
#define DUCTTAPE_FALL_HPP

#include <chrono>
#include "executive.hpp"

namespace ducttape {
    namespace stages {
        class fall : public stage_queue {
        public:
            fall(executive &exec, std::string name);
        };
    }
}

#endif //DUCTTAPE_FALL_HPP
