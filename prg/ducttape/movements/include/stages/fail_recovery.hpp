//
// Created by kuba on 22.11.19.
//

#ifndef DUCTTAPE_FAIL_RECOVERY_HPP
#define DUCTTAPE_FAIL_RECOVERY_HPP

#include "executive.hpp"

namespace ducttape {
    namespace stages {
        class fail_recovery : public stage_queue {
        public:
            fail_recovery(executive &exec, std::string name);
        };
    }
}

#endif //DUCTTAPE_FAIL_RECOVERY_HPP
