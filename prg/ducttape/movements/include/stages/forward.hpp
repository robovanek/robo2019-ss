#ifndef DUCTTAPE_FORWARD_HPP
#define DUCTTAPE_FORWARD_HPP

#include <movement_model.hpp>
#include <sensorics.hpp>
#include "executive.hpp"

namespace ducttape {
    namespace stages {
        class forward : public stage {
        public:
            forward(executive &exec, distance_type checkpoint, int amount, bool just_align, std::string &&name);

            loop operator()() final;

        private:
            bool singleWall();
            bool highWall();
            void notifyOfStop();
            loop doStart();
            sonic_result sonic();
            bool hasReachedCheckpoint();
            bool m_first;
            distance_type m_checkpointDistance;
            int m_checkpointAmount;
            bool m_justAlign;
            robofat2::logging::source m_log;
        };
    }
}

#endif //DUCTTAPE_FORWARD_HPP
