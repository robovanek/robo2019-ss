#ifndef DUCTTAPE_KAMIKAZE_HPP
#define DUCTTAPE_KAMIKAZE_HPP

#include "executive.hpp"

namespace ducttape {
    namespace stages {
        class kamikaze : public stage_queue {
        public:
            kamikaze(executive &exec, int depth, std::string name);
        };
    }
}

#endif //DUCTTAPE_KAMIKAZE_HPP
