#ifndef DUCTTAPE_INITIAL_HPP
#define DUCTTAPE_INITIAL_HPP

#include "executive.hpp"

namespace ducttape {
    namespace stages {
        class initial : public stage {
        public:
            explicit initial(executive &exec);

            loop operator()() override;
        };
    }
}

#endif //DUCTTAPE_INITIAL_HPP
