#ifndef DUCTTAPE_CLIMB_HPP
#define DUCTTAPE_CLIMB_HPP

#include "executive.hpp"

namespace ducttape {
    namespace stages {
        class climb : public stage_queue {
        public:
            climb(executive &exec, std::string name);
        };
    }
}

#endif //DUCTTAPE_CLIMB_HPP
