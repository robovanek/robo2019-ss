#ifndef DUCTTAPE_ARM_WAIT_HPP
#define DUCTTAPE_ARM_WAIT_HPP

#include "executive.hpp"

namespace ducttape {
    namespace stages {
        class arm_wait : public ducttape::stage {
        public:
            arm_wait(executive &exec, std::string name);

            loop operator()() final;
        };
    }
}

#endif //DUCTTAPE_ARM_WAIT_HPP
