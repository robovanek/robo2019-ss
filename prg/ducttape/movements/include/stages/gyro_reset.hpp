#ifndef DUCTTAPE_GYRO_RESET_HPP
#define DUCTTAPE_GYRO_RESET_HPP

#include <executive.hpp>

namespace ducttape {
    namespace stages {
        class gyro_reset : public stage {
        public:
            gyro_reset(executive &exec, align_type type, std::string &&name);

            loop operator()() override;

            static void run(executive &exec, align_type type);

        private:
            align_type m_type;
        };
    }
}

#endif //DUCTTAPE_GYRO_RESET_HPP
