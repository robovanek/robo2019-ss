#ifndef DUCTTAPE_HYBRID_WAIT_HPP
#define DUCTTAPE_HYBRID_WAIT_HPP

#include <movement_model.hpp>
#include <robofat2/utilities/stopwatch.hpp>
#include "executive.hpp"

namespace ducttape {
    namespace stages {
        enum class wait_type {
            climb_upper,
            climb_lower_pregyro,
            climb_lower_postgyro,
            fall_upper,
            fall_lower,
            pre_turn,
            post_turn,
            after_touch_contact,
            after_color_contact,
            fail,
            none
        };

        class hybrid_wait : public stage {
        public:
            hybrid_wait(executive &exec, wait_type wait, std::string name);

            loop operator()() final;
        private:
            bool distanceFinished();

            robofat2::utilities::stopwatch m_timer;
            std::chrono::nanoseconds m_duration = {};
            distance_type m_distance = {};
        };
    }
}

#endif //DUCTTAPE_HYBRID_WAIT_HPP
