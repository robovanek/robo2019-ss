#ifndef DUCTTAPE_TRAVELBACK_HPP
#define DUCTTAPE_TRAVELBACK_HPP

#include <movement_model.hpp>
#include "executive.hpp"
#include "hybrid_wait.hpp"

namespace ducttape {
    namespace stages {
        class travelback : public stage_queue {
        public:
            travelback(executive &exec, wait_type before, distance_type type, int delta, std::string name);
        };
    }
}

#endif //DUCTTAPE_TRAVELBACK_HPP
