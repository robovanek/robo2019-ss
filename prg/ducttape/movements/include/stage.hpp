#ifndef DUCTTAPE_STAGE_HPP
#define DUCTTAPE_STAGE_HPP

#include <utility>
#include <string>
#include <stdexcept>
#include <memory>
#include <list>
#include <functional>

#include <robofat2/sequencer/stage.hpp>
#include <robofat2/sequencer/stage_queue.hpp>
#include <robofat2/sequencer/command.hpp>

namespace ducttape {
    class executive;

    using robofat2::sequencer::loop;

    using stage = robofat2::sequencer::stage<executive>;
    using command = robofat2::sequencer::command<executive>;
    using stage_queue = robofat2::sequencer::stage_queue<executive>;
}

#endif //DUCTTAPE_STAGE_HPP
