#include "router_dijkstra.hpp"
#include "environment.hpp"

using namespace robofat2::maps;
using namespace robofat2::settings;
using namespace robofat2::logging;
using namespace ducttape;
using namespace ducttape::maps;
using namespace ducttape::spatial;

router_dijkstra::router_dijkstra(environment &env,
                                 settings &conf,
                                 std::shared_ptr<sink> logDevice)
        : router_base(env, conf, logDevice),
          m_shortestPathFinder(
                  env.get_map(),
                  conf.get_integer("pathfinder/weight/climb"),
                  conf.get_integer("pathfinder/weight/travel"),
                  conf.get_integer("pathfinder/weight/unknown")),
          m_peakFinder(
                  env.get_map(),
                  conf.get_float("pathfinder/peak/weight/climb"),
                  conf.get_float("pathfinder/peak/weight/travel"),
                  conf.get_float("pathfinder/peak/weight/unknown")),
          m_randomDevice(),
          m_randomEngine(m_randomDevice()),
          m_randomDistribution(0, 3),
          m_alignHelper(env, conf) {}

decision ducttape::spatial::router_dijkstra::get_next_direction() {
    m_log.info("asked for decision, mode = dijkstra");

    {
        decision alignment;
        if (m_alignHelper.process(alignment) == align_decision::do_align) {
            return alignment;
        }
    }

    switch (m_phase) {
        case phase::go2finish:
            return follow_2finish();
        case phase::go2peaks:
            return follow_2peaks();
        default:
        case phase::go2random:
            return follow_2random();
    }
}

void router_dijkstra::reset() {
    m_phase = phase::go2finish;
}

decision router_dijkstra::follow_2finish() {
    m_log.info("situation: phase = go to finish line");
    m_phase = phase::go2finish;

    auto path = m_shortestPathFinder(m_environment->get_current_position());
    switch (path.size()) {
        case 0: {
            m_log.info("decision: we are lost (no path to finish), going random");
            return follow_2random();
        }
        case 1: {
            m_log.info("decision: reached the finish line, switching to peaks phase");
            return follow_2peaks();
        }
        default: {
            decision where = first_step_of(path);
            m_log.info("decision: found path, going %s by %d",
                       orient_as_string(where.direction_where).c_str(),
                       where.blocks_forward);
            return where;
        }
    }
}

decision router_dijkstra::follow_2peaks() {
    m_log.info("situation: phase = go to hill peaks");
    m_phase = phase::go2peaks;

    auto path = m_peakFinder(m_environment->get_current_position());
    switch (path.size()) {
        case 0: {
            m_log.info("decision: we are lost (no path to finish), going random");
            return follow_2random();
        }
        case 1: {
            m_log.info("decision: reached the last unknown position, going random");
            return follow_2random();
        }
        default: {
            decision where = first_step_of(path);
            m_log.info("decision: found path, going %s by %d",
                       orient_as_string(where.direction_where).c_str(),
                       where.blocks_forward);
            return where;
        }
    }
}

decision router_dijkstra::follow_2random() {
    m_phase = phase::go2random;

    int guess = m_randomDistribution(m_randomEngine);
    orient where = maps::rotate(maps::orient::north, maps::rotation::left, guess);

    m_log.info("situation: phase = random guessing");
    m_log.info("decision: go %s", orient_as_string(where).c_str());
    return decision(where, 1);
}

decision router_dijkstra::first_step_of(std::deque<pos> &path) {
    decision result;
    result.blocks_forward = 1;
    result.direction_where = pop_waypoint(path);

    while (m_coalesceForwardMoves && path.size() >= 2) {
        orient currentDirection = pop_waypoint(path);

        if (currentDirection == result.direction_where)
            result.blocks_forward++;
        else
            break;
    }
    result.blocks_forward = m_alignHelper.maxBlocksForward(result.blocks_forward);
    return result;
}

orient router_dijkstra::pop_waypoint(std::deque<pos> &path) {
    pos first = path.front();
    path.pop_front();
    pos second = path.front();
    return first.direction_to(second);
}
