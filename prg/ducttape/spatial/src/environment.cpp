#include "environment.hpp"

#include <utility>

using namespace ducttape;
using namespace ducttape::spatial;
using namespace ducttape::maps;
using namespace robofat2::settings;
using namespace robofat2::logging;
using namespace robofat2::maps;

environment::environment(settings &conf,
                         std::shared_ptr<sink> logDevice)
        : m_uncertainty(conf),
          m_direction(robofat2::maps::orient::north),
          m_position(1000, 1000),
          m_tileMap(),
          m_distanceToConsider(conf.get_float("spatial/block_threshold")),
          m_lastWasTravelback(false),
          m_log("env", std::move(logDevice)) {}

// trivial getters & setters //

void environment::set_initial_x(int x) {
    m_position = {x, 8};
}

orient environment::get_current_direction() const {
    return m_direction;
}

pos environment::get_current_position() const {
    return m_position;
}

ducttape::maps::tile environment::get_current_tile() const {
    return m_tileMap.get(get_current_position());
}

bool environment::last_op_was_travelback() const {
    return m_lastWasTravelback;
}

ducttape::maps::hill_map &environment::get_map() {
    return m_tileMap;
}

uncertainty &environment::get_uncertainty() {
    return m_uncertainty;
}

// public recording api //

void environment::record_forward(float travelledDistance) {
    m_log.info("record: fwd move of %f mm", travelledDistance);
    m_uncertainty.add_forward(travelledDistance);
    m_lastWasTravelback = false;
    while (travelledDistance >= m_distanceToConsider) {
        travelledDistance -= 280.0f;
        m_log.info("record: enough for a block move");
        transform_next([](maps::tile current, maps::tile next) -> maps::tile {
            return current;
        });
        go_to_next();
    }
}

void environment::record_turned(orient where) {
    m_log.info("record: turn to %s", orient_as_string(where).c_str());
    m_uncertainty.add_turn(compare_directions(m_direction, where));
    m_lastWasTravelback = false;
    m_direction = where;
}

void environment::record_climb_down() {
    m_log.info("record: climb down / fall");
    m_uncertainty.add_climbdown();
    m_lastWasTravelback = false;
    transform_next([](maps::tile current, maps::tile next) -> maps::tile {
        return next.with_height(current.get_lower_bound() - 1);
    });
    go_to_next();
}

void environment::record_climb_up() {
    m_log.info("record: climb up");
    m_uncertainty.add_climbup();
    m_lastWasTravelback = false;
    transform_next([](maps::tile current, maps::tile next) -> maps::tile {
        return next.with_height(current.get_lower_bound() + 1);
    });
    go_to_next();
}

void environment::record_travelback_high(int height) {
    m_log.info("record: high travelback (>=%d blocks obstacle)", height);
    m_uncertainty.add_travelback_high();
    m_lastWasTravelback = true;
    transform_next([height](maps::tile current, maps::tile next) -> maps::tile {
        return next.with_lower_bound(current.get_lower_bound() + height);
    });
}

void environment::record_travelback_deep(int depth) {
    m_log.info("record: deep travelback (%d blocks cliff)", depth);
    m_uncertainty.add_travelback_deep();
    m_lastWasTravelback = true;
    transform_next([depth](maps::tile current, maps::tile next) -> maps::tile {
        return next.with_height(current.get_lower_bound() - depth);
    });
}

void environment::record_kamikaze(int depth) {
    m_log.info("record: kamikaze (%d blocks cliff)", depth);
    m_uncertainty.add_kamikaze(depth);
    m_lastWasTravelback = false;
    transform_next([depth](maps::tile current, maps::tile next) -> maps::tile {
        return next.with_height(current.get_lower_bound() - depth);
    });
    go_to_next();
}

void environment::record_last_climb_up_failed() {
    m_log.warn("record: last climb failed!");
    m_uncertainty.add_climb_failure();
    m_lastWasTravelback = false;
    go_to_prev();
}

void environment::record_gyro_alignment() {
    m_log.info("record: gyro realigned");
    m_uncertainty.add_gyro_realign();
}

// private recording helpers //

void environment::transform_next(transform_fn &&fn) {
    auto posCurrent = get_current_position();
    auto posNext = posCurrent.advance(get_current_direction());
    auto tileCurrent = m_tileMap.get(posCurrent);
    auto tileNext = m_tileMap.get(posNext);

    auto tileNew = fn(tileCurrent, tileNext);
    m_tileMap.set(posNext, tileNew);

    m_log.info("set: %s → %s", tileNew.as_string().c_str(), posNext.as_string().c_str());
}

void environment::go_to_next() {
    go_to(+1);
}

void environment::go_to_prev() {
    go_to(-1);
}

void environment::go_to(int fwds) {
    m_position = m_position.advance(m_direction, fwds);
    if (!m_tileMap.isIndexValid(m_position)) {
        m_log.error("currently we are out of the map bounds: %s", m_position.as_string().c_str());
    } else {
        m_log.info("move by %d → %s", fwds, m_position.as_string().c_str());
    }
}
