
#include "uncertainty.hpp"

using namespace ducttape;
using namespace robofat2::maps;

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunknown-pragmas"
#pragma ide diagnostic ignored "MemberFunctionCanBeStatic"

spatial::uncertainty::uncertainty(robofat2::settings::settings &conf)
        : m_fwdScore(conf.get_float("guesstimator/initial/fwd")),
          m_azimuthScore(conf.get_float("guesstimator/initial/azimuth")),
          m_fwdThreshold(conf.get_float("guesstimator/threshold/fwd")),
          m_azimuthThreshold(conf.get_float("guesstimator/threshold/azimuth")),
          m_coefFwdDirect(conf.get_float("guesstimator/evolve/fwd_direct")),
          m_coefFwdAzimuth(conf.get_float("guesstimator/evolve/fwd_azimuth")),
          m_coefAzimuth(conf.get_float("guesstimator/evolve/azimuth")) {}

void spatial::uncertainty::add_forward(float distance) {
    m_fwdScore += distance / 280.0f * m_coefFwdDirect;
    m_fwdScore += distance / 280.0f * m_coefFwdAzimuth * m_azimuthScore;
}

void spatial::uncertainty::add_turn(robofat2::maps::relative_position how) {
    float count;
    switch (how) {
        case relative_position::new_same:
            count = 0.0f;
            break;
        default:
        case relative_position::new_to_left:
        case relative_position::new_to_right:
            count = 1.0f;
            break;
        case relative_position::new_opposite:
            count = 2.0f;
            break;
    }
    m_azimuthScore += count * m_coefAzimuth;
}

void spatial::uncertainty::add_climbup() {
    // noop
}

void spatial::uncertainty::add_climbdown() {
    // noop
}

void spatial::uncertainty::add_climb_failure() {
    // noop
}

void spatial::uncertainty::add_gyro_realign() {
    m_fwdScore = 0.0f;
    m_azimuthScore = 0.0f;
}

void spatial::uncertainty::add_travelback_high() {
    // noop
}

void spatial::uncertainty::add_travelback_deep() {
    // noop
}

void spatial::uncertainty::add_kamikaze(int depth) {
    // noop for now
}

bool spatial::uncertainty::is_realignment_wanted() const {
    return m_fwdScore >= m_fwdThreshold ||
           m_azimuthScore >= m_azimuthThreshold;
}

float spatial::uncertainty::max_safe_forward_distance() const {
    float remainingScore = m_fwdThreshold - m_fwdScore;
    if (remainingScore <= 0.0f)
        return 0.0f;

    float distance = remainingScore / (m_coefFwdDirect + m_coefFwdAzimuth * m_azimuthScore) * 280.0f;
    return distance;
}

#pragma clang diagnostic pop
