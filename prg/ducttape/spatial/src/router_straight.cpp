#include <utility>
#include "router_straight.hpp"
#include "environment.hpp"

using namespace ducttape;
using namespace ducttape::maps;
using namespace ducttape::spatial;

router_straight::router_straight(environment &env,
                                 robofat2::settings::settings &conf,
                                 std::shared_ptr<robofat2::logging::sink> logDevice)
        : router_base(env, conf, std::move(logDevice)) {}

decision router_straight::get_next_direction() {
    m_log.info("straight: asked for decision");
    m_log.info("decision: always go north");
    if (m_coalesceForwardMoves) {
        return decision(orient::north, m_environment->get_map().height());
    } else {
        return decision(orient::north, 1);
    }
}

void router_straight::reset() {}
