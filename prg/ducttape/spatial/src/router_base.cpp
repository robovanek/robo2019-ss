#include "router_base.hpp"

#include <utility>

using namespace ducttape;
using namespace ducttape::spatial;
using namespace ducttape::maps;
using namespace robofat2::maps;
using namespace robofat2::settings;
using namespace robofat2::logging;

router_base::router_base(environment &env,
                         settings &conf,
                         std::shared_ptr<sink> logDevice)
        : m_environment(&env),
          m_coalesceForwardMoves(conf.get_bool("spatial/coalesce_fwds")),
          m_log("router", std::move(logDevice)) {}

decision::decision()
        : decision(orient::north, 1) {}

decision::decision(orient dir, int count)
        : decision(dir, count, false) {}

decision::decision(orient dir, int count, bool justAlign)
        : direction_where(dir),
          blocks_forward(count),
          just_align(justAlign) {}
