#include <cmath>
#include "uncertainty_router_helper.hpp"
#include "environment.hpp"
#include "router_base.hpp"

using namespace robofat2::maps;
using namespace robofat2::settings;
using namespace ducttape;
using namespace ducttape::spatial;
using namespace ducttape::maps;

uncertainty_router_helper::uncertainty_router_helper(environment &env,
                                                     settings &conf)
        : m_env(&env),
          m_enable(conf.get_bool("guesstimator/enable")) {}

align_decision uncertainty_router_helper::process(decision &output) {
    if (!m_enable)
        return align_decision::carry_on;

    if (m_env->get_uncertainty().is_realignment_wanted()) {
        return process(m_env->get_current_position(), output);
    } else {
        return align_decision::carry_on;
    }
}

align_decision uncertainty_router_helper::process(pos where, decision &output) {
    auto front = m_env->get_current_direction();
    auto left = rotate(front, rotation::left);
    auto right = rotate(front, rotation::right);

    if (alignPossible(front, where, output) ||
        alignPossible(left, where, output) ||
        alignPossible(right, where, output)) {
        return align_decision::do_align;
    } else {
        return align_decision::carry_on;
    }
}

unsigned int uncertainty_router_helper::maxBlocksForward(unsigned int request) {
    // if not enabled, just pass the value on
    if (!m_enable) {
        return request;
    }

    decision tmp;

    auto maxBlocks = (unsigned int) truncf(m_env->get_uncertainty().max_safe_forward_distance() / 280.0f) + 1;

    for (unsigned int blocks = maxBlocks; blocks <= request; blocks++) {
        pos stop = m_env->get_current_position().advance(m_env->get_current_direction(),
                                                         (int) blocks);
        if (process(stop, tmp) == align_decision::do_align) {
            return blocks;
        }
    }
    // fuuu, no alignment
    return request;
}

bool uncertainty_router_helper::alignPossible(orient where, pos source, decision &output) {
    auto srcPos = source;
    auto dstPos = srcPos.advance(where, 1);

    return alignPossible(where, srcPos, dstPos, output);
}

bool uncertainty_router_helper::alignPossible(orient where, pos source, pos target, decision &output) {
    auto srcTile = m_env->get_map().get(source);
    auto dstTile = m_env->get_map().get(target);

    return alignPossible(where, srcTile, dstTile, output);
}

bool uncertainty_router_helper::alignPossible(orient where, tile source, tile target, decision &output) {
    auto srcHeight = source.get_lower_bound();
    auto dstLower = target.get_lower_bound();

    if (dstLower > srcHeight) {
        output.direction_where = where;
        output.blocks_forward = 1;
        output.just_align = true;
        return true;
    }
    return false;
}
