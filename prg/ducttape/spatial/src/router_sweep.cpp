#include "router_sweep.hpp"

#include <utility>
#include "environment.hpp"

using namespace robofat2;
using namespace robofat2::maps;
using namespace ducttape;
using namespace ducttape::maps;
using namespace ducttape::spatial;

router_sweep::router_sweep(environment &env,
                           robofat2::settings::settings &conf,
                           std::shared_ptr<robofat2::logging::sink> logDevice)
        : router_base(env, conf, std::move(logDevice)),
          m_sweepToRight(true) {}

decision router_sweep::get_next_direction() {
    m_log.info("sweep: asked for decision");
    auto dir = m_environment->get_current_direction();
    auto pos = m_environment->get_current_position();
    bool hadTravelback = m_environment->last_op_was_travelback();
    int fullLength = m_coalesceForwardMoves ? (int)m_environment->get_map().height() : 1;

    if (dir == orient::north) {
        if (hadTravelback) {
            m_log.info("situation: obstacle hit while going north");
            if (pos.x() == 0) {
                m_log.info("note: left edge reached, sweeping right");
                m_sweepToRight = true;
            } else if (pos.x() == 5) {
                m_log.info("note: right edge reached, sweeping left");
                m_sweepToRight = false;
            }
            if (m_sweepToRight) {
                m_log.info("decision: go east");
                return decision(orient::east, 1);
            } else {
                m_log.info("decision: go west");
                return decision(orient::west, 1);
            }
        } else {
            m_log.info("situation: successfully went north");
            m_log.info("decision: go north again");
            return decision(orient::north, fullLength);
        }
    } else {
        if (hadTravelback) {
            m_log.info("situation: obstacle hit while going sideways");
            m_log.info("decision: go sideways to the opposite direction");
            m_sweepToRight = !m_sweepToRight;
            return decision(rotate(dir, rotation::right, 2), 2);
        } else {
            m_log.info("situation: successfully went sideways");
            m_log.info("decision: go north now");
            return decision(orient::north, fullLength);
        }
    }
}

void router_sweep::reset() {
    m_sweepToRight = m_environment->get_current_position().x() <= 2;
}
