cmake_minimum_required(VERSION 3.7)

project("DuctTape-Spatial"
        LANGUAGES C CXX)
message(STATUS "Configuring DuctTape-Spatial")

add_library(dtspatial STATIC
        src/uncertainty.cpp
        src/environment.cpp
        src/router_base.cpp
        src/router_sweep.cpp
        src/router_dijkstra.cpp
        src/router_straight.cpp
        src/uncertainty_router_helper.cpp
        include/uncertainty.hpp
        include/environment.hpp
        include/router_base.hpp
        include/router_sweep.hpp
        include/router_dijkstra.hpp
        include/router_straight.hpp
        include/uncertainty_router_helper.hpp
        )

target_include_directories(dtspatial
        PUBLIC
        $<INSTALL_INTERFACE:include>
        $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
        PRIVATE
        ${CMAKE_CURRENT_SOURCE_DIR}/src
        )

target_link_libraries(dtspatial
        PUBLIC
        RoboFAT2::settings
        RoboFAT2::logging
        DuctTape::maps
        )

set_target_properties(dtspatial PROPERTIES
        CXX_STANDARD 11
        CXX_STANDARD_REQUIRED YES
        CXX_EXTENSIONS NO
        )

add_library(DuctTape::spatial ALIAS dtspatial)
