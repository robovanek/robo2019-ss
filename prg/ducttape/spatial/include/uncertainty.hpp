#ifndef DUCTTAPE_UNCERTAINTY_HPP
#define DUCTTAPE_UNCERTAINTY_HPP

#include <robofat2/settings/settings.hpp>
#include <robofat2/maps/orientation.hpp>
#include <ostream>

namespace ducttape {
    namespace spatial {
        class uncertainty {
        public:
            explicit uncertainty(robofat2::settings::settings &conf);

            void add_forward(float distance);
            void add_turn(robofat2::maps::relative_position how);
            void add_climbup();
            void add_climbdown();
            void add_climb_failure();
            void add_gyro_realign();
            void add_travelback_high();
            void add_travelback_deep();
            void add_kamikaze(int depth);

            bool is_realignment_wanted() const;
            float max_safe_forward_distance() const;

        private:
            float m_fwdScore;
            float m_azimuthScore;

            float m_fwdThreshold;
            float m_azimuthThreshold;

            float m_coefFwdDirect;
            float m_coefFwdAzimuth;
            float m_coefAzimuth;
        };
    }
}

#endif //DUCTTAPE_UNCERTAINTY_HPP
