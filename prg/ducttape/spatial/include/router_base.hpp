#ifndef DUCTTAPE_ROUTER_BASE_HPP
#define DUCTTAPE_ROUTER_BASE_HPP

#include <hill_map.hpp>
#include <robofat2/settings/settings.hpp>
#include <robofat2/logging/sink.hpp>
#include <robofat2/logging/source.hpp>

namespace ducttape {
    namespace spatial {
        class environment;
        struct decision {
            decision();
            explicit decision(robofat2::maps::orient dir, int count);
            explicit decision(robofat2::maps::orient dir, int count, bool justAlign);

            robofat2::maps::orient direction_where;
            unsigned int blocks_forward;
            bool just_align;
        };

        class router_base {
        public:
            explicit router_base(environment &env,
                                 robofat2::settings::settings &conf,
                                 std::shared_ptr<robofat2::logging::sink> logDevice);

            virtual ~router_base() = default;

            virtual decision get_next_direction() = 0;

            virtual void reset() = 0;

        protected:
            environment *m_environment;
            bool m_coalesceForwardMoves;
            robofat2::logging::source m_log;
        };
    }
}

#endif //DUCTTAPE_ROUTER_BASE_HPP
