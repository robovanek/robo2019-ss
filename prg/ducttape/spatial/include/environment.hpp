#ifndef DUCTTAPE_ENVIRONMENT_HPP
#define DUCTTAPE_ENVIRONMENT_HPP


#include <robofat2/settings/settings.hpp>
#include <robofat2/logging/sink.hpp>
#include <robofat2/logging/source.hpp>
#include <hill_map.hpp>
#include <functional>
#include <memory>
#include "uncertainty.hpp"

namespace ducttape {
    namespace spatial {
        class environment {
        public:
            explicit environment(robofat2::settings::settings &conf,
                                 std::shared_ptr<robofat2::logging::sink> logDevice);

            void set_initial_x(int x);

            void record_forward(float travelledDistance);

            void record_turned(robofat2::maps::orient where);

            void record_climb_down();

            void record_climb_up();

            void record_travelback_high(int height);

            void record_travelback_deep(int depth);

            void record_last_climb_up_failed();

            void record_kamikaze(int depth);

            void record_gyro_alignment();

            robofat2::maps::orient get_current_direction() const;

            robofat2::maps::pos get_current_position() const;

            ducttape::maps::tile get_current_tile() const;

            bool last_op_was_travelback() const;

            ducttape::maps::hill_map &get_map();

            uncertainty &get_uncertainty();

        private:
            using transform_fn = std::function<maps::tile(maps::tile, maps::tile)>;

            void transform_next(transform_fn &&fn);

            void go_to_next();

            void go_to_prev();

            void go_to(int fwds);

        private:
            uncertainty m_uncertainty;
            robofat2::maps::orient m_direction;
            robofat2::maps::pos m_position;
            ducttape::maps::hill_map m_tileMap;
            float m_distanceToConsider;
            bool m_lastWasTravelback;
            robofat2::logging::source m_log;
        };
    }
}

#endif //DUCTTAPE_ENVIRONMENT_HPP
