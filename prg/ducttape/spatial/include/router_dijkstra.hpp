#ifndef DUCTTAPE_ROUTER_DIJKSTRA_HPP
#define DUCTTAPE_ROUTER_DIJKSTRA_HPP

#include <hill_map.hpp>
#include <hill_dijkstra.hpp>
#include <hill_peak_dijkstra.hpp>
#include <random>
#include "router_base.hpp"
#include "uncertainty_router_helper.hpp"

namespace ducttape {
    namespace spatial {
        enum class phase {
            go2finish,
            go2peaks,
            go2random
        };

        class environment;

        class router_dijkstra : public router_base {
        public:
            router_dijkstra(environment &env,
                    robofat2::settings::settings &conf,
                    std::shared_ptr<robofat2::logging::sink> logDevice);

            decision get_next_direction() override;

            void reset() override;

            decision follow_2finish();
            decision follow_2peaks();
            decision follow_2random();

        private:
            decision first_step_of(std::deque<maps::pos> &path);
            static maps::orient pop_waypoint(std::deque<maps::pos> &path);

            ducttape::maps::hill_dijkstra m_shortestPathFinder;
            ducttape::maps::hill_peak_dijkstra m_peakFinder;
            phase m_phase = phase::go2finish;
            std::random_device m_randomDevice;
            std::default_random_engine m_randomEngine;
            std::uniform_int_distribution<int> m_randomDistribution;
            uncertainty_router_helper m_alignHelper;
        };
    }
}

#endif //DUCTTAPE_ROUTER_DIJKSTRA_HPP
