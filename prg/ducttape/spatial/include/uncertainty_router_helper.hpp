#ifndef DUCTTAPE_UNCERTAINTY_ROUTER_HELPER_HPP
#define DUCTTAPE_UNCERTAINTY_ROUTER_HELPER_HPP

#include <hill_map.hpp>
#include <robofat2/settings/settings.hpp>

namespace ducttape {
    namespace spatial {
        class environment;

        class decision;

        enum class align_decision {
            do_align,
            carry_on
        };

        class uncertainty_router_helper {
        public:
            explicit uncertainty_router_helper(environment &env,
                                               robofat2::settings::settings &conf);

            align_decision process(decision &output);

            unsigned int maxBlocksForward(unsigned int request);

        private:
            align_decision process(robofat2::maps::pos where, decision &output);

            bool alignPossible(robofat2::maps::orient where,
                               robofat2::maps::pos source,
                               decision &output);

            bool alignPossible(robofat2::maps::orient where,
                               robofat2::maps::pos source,
                               robofat2::maps::pos target,
                               decision &output);

            static bool alignPossible(robofat2::maps::orient where,
                                      ducttape::maps::tile source,
                                      ducttape::maps::tile target,
                                      decision &output);

        private:
            environment *m_env;
            bool m_enable;
        };
    }
}

#endif //DUCTTAPE_UNCERTAINTY_ROUTER_HELPER_HPP
