#ifndef DUCTTAPE_ROUTER_STRAIGHT_HPP
#define DUCTTAPE_ROUTER_STRAIGHT_HPP

#include "router_base.hpp"

namespace ducttape {
    namespace spatial {
        class environment;
        class router_straight : public router_base {
        public:
            explicit router_straight(environment &env,
                                     robofat2::settings::settings &conf,
                                     std::shared_ptr<robofat2::logging::sink> logDevice);

            decision get_next_direction() override;

            void reset() override;
        };
    }
}

#endif //DUCTTAPE_ROUTER_STRAIGHT_HPP
