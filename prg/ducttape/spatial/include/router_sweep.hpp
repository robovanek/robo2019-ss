#ifndef DUCTTAPE_ROUTER_SWEEP_HPP
#define DUCTTAPE_ROUTER_SWEEP_HPP

#include "router_base.hpp"

namespace ducttape {
    namespace spatial {
        class environment;

        class router_sweep : public router_base {
        public:
            explicit router_sweep(environment &env,
                                  robofat2::settings::settings &conf,
                                  std::shared_ptr<robofat2::logging::sink> logDevice);
            decision get_next_direction() override;

            void reset() override;

        private:
            bool m_sweepToRight;
        };
    }
}

#endif //DUCTTAPE_ROUTER_SWEEP_HPP
