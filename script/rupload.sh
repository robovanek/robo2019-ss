#!/bin/bash
DIR="$( cd "$( dirname $( readlink -f "${BASH_SOURCE[0]}" ) )/../prg" >/dev/null 2>&1 && pwd )"
IMAGE="registry.gitlab.com/robovanek/robofat2/builder-arm:latest"

LOCAL_PATH="$DIR/build/ducttape/ducttape"
REMOTE_PATH="/home/robot/a_program"
REMOTE_HOSTNAME="ducttape"

scp "$LOCAL_PATH" "$REMOTE_HOSTNAME:$REMOTE_PATH"
ssh "$REMOTE_HOSTNAME" sudo setcap cap_sys_nice+ep "$REMOTE_PATH"
